/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.controleur.adf;

import org.sofiframework.constante.Constantes;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import oracle.jbo.ApplicationModule;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Objet servant de base pour un requestProcessor de Struts.
 * <p>
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class BaseRequestProcessor extends org.sofiframework.presentation.struts.controleur.BaseRequestProcessor {
  /** Variable désignant le log à utiliser pour cette classe */
  protected static Log log = LogFactory.getLog(org.sofiframework.presentation.struts.controleur.BaseRequestProcessor.class);

  /**
   * Méthode appelée avant chaque exécution de requête.
   * <p>
   * Cette méthode sert à prendre le module d'application (en appelant la méthode getModele) et à le mettre
   * dans la request de la requête. Ainsi on s'assure que le module d'application est toujours disponible tout au long du traitement de la requête.
   * <p>
   * @param request La requête HTTP en traitement
   * @param response La réponse HTTP à fournir
   * @return boolean Cette méthode retourne toujours la valeur "true" ce qui veut dire que l'exécution de la requête doit continuer
   */
  protected boolean processPreprocess(HttpServletRequest request,
    HttpServletResponse response) {
    // Accéder au modèle de l'application.
    ApplicationModule modele = (ApplicationModule) this.getModele(request,
        response);

    // Placer l'Application Module dans le request.
    request.setAttribute(Constantes.MODELE, modele);

    return true;
  }

  /**
  * Méthode qui permet de se connecter sur un Application Module sur le serveur d'application.
  * <p>
  * Cette méthode sert à aller reprendre l'Application Module réservé pour un utilisateur (selon
  * son numéro de session) sur le serveur d'application, si le mode de libération est Stateful. Sinon
  * s'il est Stateless, il va emprunter un quelquonque Application Module
  * Vous devez inscrire le nom de l'application  module principal dans le fichier web.xml
  * Le nom du paramètre doit se nommer: appId<p>
  * Voici un exemple:<br>
  * &nbsp;&lt;init-param&gt;
  * &nbsp;&nbsp;&lt;param-name&gt;modele&lt;/param-name&gt;<br>
  * &nbsp;&nbsp;&lt;param-value&gt;ModeleXXX&lt;/param-value&gt;<br>
  * &nbsp;&lt;/init-param&gt;<br>
  * <p>
  * @param request La requête HTTP en traitement
  * @param response La réponse HTTP à fournir
  * @return Application Module L'Application Module BC4J servant de modèle.
  */
  protected Object getModele(HttpServletRequest request,
    HttpServletResponse response) {
    return super.getModele(request, response);
  }

  /**
   * Libère le module d'application BC4J.
   * <p>
   * Cette méthode libère le module d'application BC4J. Ce dernier restera réservé pour l'utilisateur
   * tant et aussi longtemps que la session de ce dernier ne sera pas morte.
   * <p>
   * @param request La requête HTTP en traitement
   * @param response La réponse HTTP à fournir
   */
  protected void libererModele(HttpServletRequest request,
    HttpServletResponse response) {
    super.libererModele(request, response);
  }
}
