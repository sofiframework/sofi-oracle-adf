/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.presentation.struts.controleur.adf;

import java.io.IOException;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.application.message.GestionMessage;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.constante.Constantes;
import org.sofiframework.constante.ConstantesMessage;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.objetstransfert.ObjetCleValeur;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.presentation.utilitaire.Href;
import org.sofiframework.utilitaire.UtilitaireHttp;
import org.sofiframework.utilitaire.UtilitaireString;
import org.sofiframework.utilitaire.documentelectronique.DocumentElectronique;


/**
 * Action qui généralise le traitement effectué dans les pages qui appelent
 * des rapports via une requête HTTP.
 * <p>
 * @author Jean-Maxime Pelletier, Nurun inc.
 * @author Jean-François Brassard, Nurun inc.
 * @version SOFI 1.1
 */
public abstract class BaseRapportUrlAction extends BaseDispatchAction {
  /** Constructeur par défault */
  public BaseRapportUrlAction() {
  }

  public ActionForward acceder(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {
    request.getSession().removeAttribute(Constantes.DOCUMENT_PDF);

    if (form != null) {
      ((BaseForm) form).initialiserFormulaire(request, mapping,
          new ObjetCleValeur(), true);
    }

    return mapping.findForward(this.getNomVueRetour());
  }

  /**
   * Action permettant de générer un rapport Oracle Rapport de format PDF.
   * C'est l'action qui doit être appelée par le bouton "Générer Rapport".
   * @param mapping ActionMapping utilisé pour trouver cette action.
   * @param form Formulaire correspondant à l'action (facultatif).
   * @param request Requête HTP qui est traitée.
   * @param response Réponse HTTP qui est traitée.
   * @return Nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward genererRapport(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {
    this.specifierUrl(form, request);

    String urlStr = genererUrlRapport(request);

    BaseForm formulaire = (BaseForm) form;

    if (urlStr != null) {
      URL url = new URL(urlStr);
      UtilitaireHttp http = new UtilitaireHttp();
      http.setIsSecure(urlStr.indexOf("https") != -1);
      http.setUrlDomaine(url.getHost());
      http.setUrlPath(url.getPath() + "?" + url.getQuery());
      http.setUrlPort(url.getPort());

      byte[] contenuPdf = http.getContenuUrl();
      DocumentElectronique document = new DocumentElectronique();
      document.setNomFichier(this.getNomFichierRapport());
      document.setCodeTypeMime("pdf");
      document.setValeurDocument(contenuPdf);

      // Fixer le document dans la session temporairement pour l'action.
      setDocumentElectronique(document, request);
      ServletOutputStream out = response.getOutputStream();
      response.setContentType("application/download");
      response.setHeader("Content-Disposition", "attachment;filename=\"" + document.getNomFichier() + "\"");
      try
      {
        out.write(document.getValeurDocument());
      } catch (Exception ex) {
        log.error(ex);
        formulaire.ajouterMessageErreurGeneral(ex.getMessage(), request);
      } finally {
        try {
          out.close();
        } catch (IOException ex) {}
      }


      if ((contenuPdf != null) && (contenuPdf.length > 0)) {
        // Traiter le message d'opération général traité avec succès
        String cleClient = GestionMessage.getInstance().getFichierConfiguration()
            .get(ConstantesMessage.INFORMATION_OPERATION_RAPPORT_SUCCES,
                request);

        if (!UtilitaireString.isVide(cleClient)) {
          formulaire.ajouterMessageInformatif(cleClient, request);
        }
      }
    }

    return mapping.findForward(this.getNomVueRetour());
  }

  /**
   * Permet d'ajouter des paramètres à un URL.
   * @param parametre le nom de paramètre
   * @param valeur la valeur du paramètre.
   * @param request la requête HTTP en traitement.
   * @return l'url avec les paramètres associés.
   */
  public Href ajouterParametreUrl(String parametre, Object valeur,
      HttpServletRequest request) {
    Href lienURLRapport = getUrlRapportTraitement(request);

    if (lienURLRapport == null) {
      throw new SOFIException(
          "Vous devez appeler setUrlRapport avant d'appeler ajouterParametreUrl");
    }

    if ((valeur != null) &&
        (String.class.isInstance(valeur) &&
            !UtilitaireString.isVide(valeur.toString()))) {
      lienURLRapport.ajouterParametre(parametre, valeur);
    }

    return lienURLRapport;
  }

  /**
   * Spécifier l'url de base qui permet d'appeler le rapport.
   * @param urlRapport l'url de base qui permet d'appeler le rapport.
   * @param avecSecurite true si vous désirez soit sécurisé pour l'utilisateur. Si vous
   * vous utilise un certficat, il sera automatiquement ajouter à l'url. Si vous utiliser un code
   * utilisateur, vous devez configurer en paramètre système le paramètre <code> paramRapportCodeUtilisateur</code> pour
   * spécifier le paramètre qui sera associé le code utilisateur.
   * @param request la requête HTTP en traitement.
   */
  public void setUrlRapport(String nomRapport, boolean avecSecurite,
      HttpServletRequest request) {
    String urlRapport = GestionParametreSysteme.getInstance()
        .getParametreSystemeEJB(nomRapport);

    //String urlRapport = "http://dv01.intrameq/reports/rwservlet?UNOU011011DEV01&destype=cache&desformat=pdf&PNU_NO_SESN_UTILS_RAPRT=420&report=UNO030403R.rep&paramform=NO&UNOU011011DEV01=&PNU_NO_SESN_UTILS=87";
    if (UtilitaireString.isVide(urlRapport)) {
      throw new SOFIException(
          "La clé du rapport n'existe pas dans les paramètres système");
    }

    Href href = new Href(urlRapport);

    request.setAttribute("urlRapportTraitement", href);

    if (avecSecurite) {
      String paramCodeUtilisateur = GestionParametreSysteme.getInstance()
          .getString(ConstantesParametreSysteme.PARAMETRE_RAPPORT_CODE_UTILISATEUR);

      if (!UtilitaireString.isVide(paramCodeUtilisateur)) {
        ajouterParametreUrl(paramCodeUtilisateur, getCertificat(request),
            request);
      } else {
        ajouterParametreUrl(GestionSecurite.getInstance()
            .getNomCookieCertificat(),
            getCertificat(request), request);
      }
    }
  }

  /**
   * Génère l'url pour appeller le rapport.
   * @param request la requête en traitement.
   * @return l'url pour appeller le rapport.
   */
  public String genererUrlRapport(HttpServletRequest request) {
    Href lienURLRapport = getUrlRapportTraitement(request);

    return lienURLRapport.getUrlPourRedirection();
  }

  private Href getUrlRapportTraitement(HttpServletRequest request) {
    Href lienURLRapport = (Href) request.getAttribute("urlRapportTraitement");

    if (lienURLRapport == null) {
    }

    return lienURLRapport;
  }

  /**
   * Dans cette méthode, vous devez implémenter cette méthode afin de specifier l'url qui va
   * appeler le rapport.
   * @param form le formulaire en traitement
   * @param request la requête HTTP en traitement.
   * @return l'url qui va appeler le rapport.
   */
  protected abstract void specifierUrl(ActionForm form,
      HttpServletRequest request);

  /**
   * Dans cette méthode, vous devez spécifier le nom logique d'appel de la vue.
   * @return le nom logique de la vue a appeller.
   */
  protected abstract String getNomVueRetour();

  /**
   * Dans cette méthode, vous devez spécifier le nom du fichier du rapport.
   * @return le nom du fichier du rapport.
   */
  protected abstract String getNomFichierRapport();
}
