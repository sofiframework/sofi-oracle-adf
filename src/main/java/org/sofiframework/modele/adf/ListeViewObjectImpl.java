/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.adf;

import oracle.jbo.Row;

/**
 * Classe pour les objets d'accès aux données destinés aux listes de navigations.
 *
 * Donne essentiellement les mêmes services que la classe BaseViewObjectImpl et
 * comporte des optimisations spécialisées permettant de trancher en deux le temps
 * d'exécution de la procédure de requête des données.
 * 
 * @see BaseViewObjectImpl, ListeViewRowImpl
 * 
 * @author Steven Charette (Nurun inc.)
 * @version SOFI 2.0
 */
public class ListeViewObjectImpl extends BaseViewObjectImpl  {
  public static final String OPTIMIZER_COLUMN_NAME = "OptimizerCount";

  private boolean removeFirstRowsHint = true;
  private long estimatedRowCount = -1;
  /**
   * @override
   */
  @Override
  protected void create() {
    super.create();
    addDynamicAttribute(OPTIMIZER_COLUMN_NAME);
  }

  /**
   * @override
   */
  @Override
  protected String buildQuery(int noUserParams, boolean forRowCount) {
    String        query = super.buildQuery(noUserParams, forRowCount);
    String        nestedQuery = getNestedQueryStmt();
    StringBuffer  buffer = new StringBuffer(query);

    int index = -1;
    if(nestedQuery == null){//requête simple.
      buffer.insert(0, "SELECT OPTIMIZER.*, COUNT(1) OVER() as " + OPTIMIZER_COLUMN_NAME + " FROM (");
      buffer.append(" )OPTIMIZER");
    }else if(query.indexOf("QRSLT") == -1){//requête composée sans where clause dynamique.
      index = buffer.indexOf(nestedQuery);
      buffer.insert(index, " SELECT OPTIMIZER.*, COUNT(1) OVER() as " + OPTIMIZER_COLUMN_NAME + " FROM (");
      index = buffer.indexOf(nestedQuery);
      buffer.insert(index + nestedQuery.length(), " )OPTIMIZER ");
    }else{// requête composée avec where clause dynamique.
      index = buffer.indexOf(nestedQuery);
      String previousSelectIndex = buffer.substring(0, index);
      index = previousSelectIndex.lastIndexOf("*");
      buffer.replace(index, index + 1, " QRSLT.*, COUNT(1) OVER() as " + OPTIMIZER_COLUMN_NAME + " ");
    }

    if(isRemoveFirstRowsHint()){
      index = buffer.indexOf("/*+ FIRST_ROWS */");
      if(index > -1){
        buffer.replace(index, index + "/*+ FIRST_ROWS */".length(), "");
      }
    }

    return buffer.toString();
  }

  /**
   * Obtenir la sous requête de la requête principale telle que défini dans
   * la vue (objet d'accès de données).
   * @return
   */
  public String getNestedQueryStmt(){
    if(getUserDefinedQuery() != null && getFullSqlMode() == 1) {
      return getUserDefinedQuery();
    } else {
      return getViewDef().getQuery();
    }
  }



  /**
   * Fixer l'attribut permettant de dire si les hints sql "First_Rows" sont à
   * proscrire.
   * @param removeFirstRowsHint true si proscrire, false autrement.
   */
  public void setRemoveFirstRowsHint(boolean removeFirstRowsHint) {
    this.removeFirstRowsHint = removeFirstRowsHint;
  }

  /**
   * Obtenir l'attribut permettant de dire si les hints sql "First_Rows" sont à
   * proscrire.
   * @return
   */
  public boolean isRemoveFirstRowsHint() {
    return removeFirstRowsHint;
  }

  /**
   * Fixer le nombre d'éléments estimés de la liste.
   * @param estimatedRowCount
   */
  public void setEstimatedRowCount(long estimatedRowCount) {
    this.estimatedRowCount = estimatedRowCount;
  }

  /**
   * @override
   */
  @Override
  public long getEstimatedRowCount() {

    if(estimatedRowCount < 0){//compute estimated row count.
      Row[] rows = getAllRowsInRange();
      if(rows != null && rows.length > 0){
        estimatedRowCount = ((Number)rows[0].getAttribute(OPTIMIZER_COLUMN_NAME)).longValue();
      }
    }
    return estimatedRowCount;
  }
}