/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.adf;

import oracle.jbo.JboException;
import oracle.jbo.server.DBTransactionImpl2;
import oracle.jbo.server.TransactionEvent;

import org.sofiframework.modele.adf.exception.ModeleADFException;

/**
 * Implémentation personnalisé de la classe DBTransaction de base.
 * @author Jean-françois Brassard (Nurun inc.)
 * @version SOFI 1.0
 */
public class TransactionBDImpl extends DBTransactionImpl2   {
  /**
   * Réécriture des méthodes de base de framework BC4J pour que les
   * exception du cycle post/commit intéprète les contraintes
   * d'intégrité de la base de de données.
   */
  @Override
  public void postChanges(TransactionEvent te) throws ModeleADFException {
    try {
      super.postChanges(te);
    }
    catch (JboException jboex) {
      String dbConstraint = org.sofiframework.modele.adf.exception.UtilitaireExceptionOracle.violationContrainteBD(jboex.getDetails());
      if (dbConstraint == null) {
        throw new ModeleADFException(jboex.getMessage());
      }
      else {
        String message = "Erreur de la base données: " + dbConstraint;
        throw new ModeleADFException(message);
      }
    }
  }
}