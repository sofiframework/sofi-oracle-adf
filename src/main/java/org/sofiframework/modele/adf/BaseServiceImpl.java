/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.adf;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.EJBObject;

import oracle.jbo.ApplicationModule;
import oracle.jbo.JboException;
import oracle.jbo.Row;
import oracle.jbo.RowInconsistentException;
import oracle.jbo.RowIterator;
import oracle.jbo.Session;
import oracle.jbo.server.ApplicationModuleImpl;
import oracle.jbo.server.EntityImpl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;
import org.apache.velocity.tools.generic.DateTool;
import org.apache.velocity.tools.generic.EscapeTool;
import org.sofiframework.application.cache.GestionCache;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.composantweb.liste.Filtre;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.constante.Constantes;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.modele.Service;
import org.sofiframework.modele.adf.generique.BaseServiceGeneriqueImpl;
import org.sofiframework.modele.adf.generique.common.BaseServiceGenerique;
import org.sofiframework.modele.adf.utilitaire.UtilitaireJDBC;
import org.sofiframework.modele.adf.utilitaire.UtilitairePLSQL;
import org.sofiframework.modele.distant.GestionServiceDistant;
import org.sofiframework.modele.exception.ConcurrenceException;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.modele.exception.ObjetTransfertNotFoundException;
import org.sofiframework.modele.securite.GestionnaireUtilisateur;
import org.sofiframework.modele.utilitaire.UtilitaireSql;
import org.sofiframework.objetstransfert.Identifiant;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.utilitaire.CaseInsensitiveMap;
import org.sofiframework.utilitaire.UtilitaireObjet;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Classe de base pour les services, permet de populer un objet de transfert
 * automatiquement via une entité de persistance ou une rangée d'un objet
 * d'accès aux données.
 * 
 * Permet aussi de populer une rangée d'un objet d'accèes de données via un
 * objet de transfert automatiquement en appliquant les formatter approprié.
 * <p>
 * 
 * @author Jean-François Brassard (Nurun inc.)
 * @author Jean-Maxime Pelletier (Nurun inc.)
 * @version 1.0
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public class BaseServiceImpl extends ApplicationModuleImpl implements Service  {
  /**
   * Instance de journalisation.
   */
  private static final Log log = LogFactory.getLog(BaseServiceImpl.class);

  /**
   * La liste des attributs temporaires pour la connexion d'un service.
   */
  private HashMap listeAttributTemporaire = new HashMap();

  @Override
  protected void afterConnect() {
    this.initialiserGestionnaires();

    // Initialiser la liste des attributs après la connexion du service.
    listeAttributTemporaire = new HashMap();
    super.afterConnect();
  }

  public void initialiserGestionnaires() {
    String nomClasseModele = super.getClass().toString();
    synchronized (GestionParametreSysteme.getInstance()) {
      if (!GestionParametreSysteme.getInstance()
          .isGestionParametreSystemeActif(super.getName().toString())) {
        String nomFichierParametres = (String) getSession().getEnvironment()
            .get("fichierParametres");

        if (nomFichierParametres == null) {
          nomFichierParametres = "modele-parametre-sofi.xml";
        }

        InputStream in = getClass().getResourceAsStream(nomFichierParametres);

        if (in != null) {
          try {
            GestionParametreSysteme.getInstance().chargerFichierParametres(in);
          } finally {
            try {
              in.close();
            } catch (IOException e) {
            }
          }
        }
      }
    }

    this.initialiserGestionCache();

    // Fixer le nom de la classe du modèle (service principal).
    if (GestionParametreSysteme.getInstance().getParametreSysteme(
        ConstantesParametreSysteme.NOM_CLASSE_MODELE) == null) {
      GestionParametreSysteme.getInstance().ajouterParametreSysteme(
          ConstantesParametreSysteme.NOM_CLASSE_MODELE, nomClasseModele);
    }

    String fichierConfigurationServiceEJB = GestionParametreSysteme
        .getInstance().getString(
            ConstantesParametreSysteme.FICHIER_CONFIG_SERVICE_EJB);

    if (!UtilitaireString.isVide(fichierConfigurationServiceEJB)) {

      // Construire la liste des contextes des services distants.
      GestionServiceDistant.construireListeContextes();
    }
  }

  /**
   * 
   * @param nomClasseModele
   */
  public void initialiserGestionCache() {
    if (!GestionCache.getInstance().isObjetCacheActif()) {
      synchronized (GestionCache.class) {
        InputStream in = getClass()
            .getResourceAsStream("application-cache.xml");

        if (in != null) {
          try {
            GestionCache.getInstance().initialiserObjetCache();
            GestionCache.getInstance().chargerDocumentXml(in);
            GestionCache.getInstance().construire(this);
          } catch (Exception e) {
            log.error("Echec lors du chargement de la cache", e);
          } finally {
            try {
              in.close();
            } catch (IOException e) {
            }
          }
        }
      }
    }
  }

  /**
   * Permet de populer un objet de transfert avec l'aide d'une rangée d'un objet
   * d'accès de donnéees.
   * 
   * @param rangee
   *          la rangée servant de source.
   * @param objetTransfert
   *          l'objet de transfert (destination).
   * @throws org.sofiframework.modele.exception.ModeleException
   *           une exception du modèle
   */
  public ObjetTransfert populerObjetTransfert(Row rangee,
      ObjetTransfert objetTransfert) throws ModeleException {
    Map listeAttributsAvecValeur = representation(rangee);
    Iterator listeAttributsAvecValeurIter = listeAttributsAvecValeur.keySet()
        .iterator();

    while (listeAttributsAvecValeurIter.hasNext()) {
      String nomAttribut = (String) listeAttributsAvecValeurIter.next();
      Object valeurAttribut = listeAttributsAvecValeur.get(nomAttribut);

      try {
        Class type = UtilitaireObjet.getClasse(rangee, nomAttribut);

        if (!EntityImpl.class.isAssignableFrom(type)
            && !RowIterator.class.isAssignableFrom(type)
            && !objetTransfert.isAttributAIgnorer(nomAttribut)) {
          convertirEnObjetTransfert(type, nomAttribut, valeurAttribut,
              objetTransfert);
        }
      } catch (ModeleException e) {
        throw e;
      }
    }

    return objetTransfert;
  }

  /**
   * Permet de populer un objet de transfert avec l'aide d'une entité de
   * persistance.
   * 
   * @param entite
   *          l'entité de persistance servant de source.
   * @param objetTransfert
   *          l'objet de transfert (destination).
   * @throws org.sofiframework.modele.exception.ModeleException
   *           une exception du modèle
   */
  protected ObjetTransfert populerObjetTransfert(EntityImpl entite,
      ObjetTransfert objetTransfert) throws ModeleException {
    Map listeAttributsAvecValeur = representation(entite);
    Iterator listeAttributsAvecValeurIter = listeAttributsAvecValeur.keySet()
        .iterator();

    while (listeAttributsAvecValeurIter.hasNext()) {
      String nomAttribut = (String) listeAttributsAvecValeurIter.next();
      Object valeurAttribut = listeAttributsAvecValeur.get(nomAttribut);

      if (!objetTransfert.isAttributAIgnorer(nomAttribut)) {
        try {
          Class type = UtilitaireObjet.getClasse(entite, nomAttribut);
          convertirEnObjetTransfert(type, nomAttribut, valeurAttribut,
              objetTransfert);
        } catch (ModeleException e) {
          throw e;
        }
      }
    }

    return objetTransfert;
  }

  /**
   * Indique si le paramètre système "traiterObjetTransfertNonModifie" est vrai
   * ou faux.
   */
  private static boolean traiterObjetTransfertNonModifie() {
    Boolean valeur = GestionParametreSysteme.getInstance().getBoolean(
        "traiterObjetTransfertNonModifie");

    return (valeur != null) && valeur.booleanValue();
  }

  /**
   * Permet de populer une rangée avec l'aide d'un objet de transfert.
   * 
   * @param objetTransfert
   *          l'objet de transfert servant de source.
   * @param rangee
   *          la rangée à populer (destination).
   * @throws org.sofiframework.modele.exception.ModeleException
   *           une exception du modèle
   */
  protected ObjetTransfert populerRangee(ObjetTransfert objetTransfert,
      Row rangee) throws ModeleException {
    // Populer seulement si l'objet de transfert à été modifié.
    if (objetTransfert.isModifie() || traiterObjetTransfertNonModifie()) {
      Map listeAttributsAvecValeur = representation(objetTransfert);
      Iterator listeAttributsAvecValeurIter = listeAttributsAvecValeur.keySet()
          .iterator();

      while (listeAttributsAvecValeurIter.hasNext()) {
        String nomAttribut = (String) listeAttributsAvecValeurIter.next();

        Object valeurAttribut = null;

        try {
          valeurAttribut = UtilitaireObjet.getValeurAttribut(objetTransfert,
              nomAttribut);
        } catch (Exception e) {
        }

        if (objetTransfert.isAttributModifie("TOUS_LES_ATTRIBUTS")
            || objetTransfert.isAttributModifie(nomAttribut)
            || (objetTransfert.getAttributsModifies().size() == 0)) {
          Class typeObjetTransfert = UtilitaireObjet.getClasse(objetTransfert,
              nomAttribut);

          if (typeObjetTransfert != null) {
            if (HashMap.class.isAssignableFrom(typeObjetTransfert)
                && nomAttribut.equals("infoSupplementaire")
                && (valeurAttribut != null)) {
              // Parcourir le HashMap afin de populer les attributs dans la
              // rangee.
              HashMap infoSupplementaire = (HashMap) valeurAttribut;
              Iterator iterateur = infoSupplementaire.keySet().iterator();

              while (iterateur.hasNext()) {
                String nomAttributInfoSuppl = (String) iterateur.next();
                Object valeurAttributInfoSuppl = infoSupplementaire
                    .get(nomAttributInfoSuppl);

                try {
                  Class type = UtilitaireObjet.getClasse(rangee,
                      nomAttributInfoSuppl);

                  if (type != null) {
                    convertirEnRangee(type, nomAttributInfoSuppl,
                        valeurAttributInfoSuppl, rangee);
                  }
                } catch (ModeleException e) {
                  throw e;
                }
              }
            } else {
              try {
                Class type = UtilitaireObjet.getClasse(rangee, nomAttribut);

                if (type != null) {
                  convertirEnRangee(type, nomAttribut, valeurAttribut, rangee);
                }
              } catch (ModeleException e) {
                throw e;
              }
            }
          }
        }
      }
    }

    return objetTransfert;
  }

  /**
   * Retourne une liste contenant les valeurs provenant de l'objet, correspond
   * au nom des champs.
   * 
   * @param objet
   *          l'objet traité
   * @return Iterator un iterateur contenant les noms d'attributs de l'objet en
   *         traitement.
   * @throws org.sofiframework.modele.exception.ModeleException
   *           une exception du modèle
   */
  protected Map representation(Object objet) {
    Map valeurs = null;

    try {
      valeurs = UtilitaireObjet.decrire(objet);

      if (valeurs.get("structureDef") != null) {
        valeurs.remove("attributesValues");
        valeurs.remove("entities");
        valeurs.remove("attributeNames");
        valeurs.remove("entities");
        valeurs.remove("class");
        valeurs.remove("XMLElementTag");
        valeurs.remove("applicationModule");
        valeurs.remove("attributeCount");
        valeurs.remove("structureDef");
        valeurs.remove("elementTagName");
        valeurs.remove("key");
        valeurs.remove("handle");
        valeurs.remove("viewObject");
        valeurs.remove("attributeCount");
        valeurs.remove("viewDef");
        valeurs.remove("attributeValues");
        valeurs.remove("newRowState");
        valeurs.remove("entityCount");
      }
    } catch (Exception e) {
      throw new ModeleException(e);
    }

    return valeurs;
  }

  /**
   * Convertit un type d'attribut provenant d'un entité ou d'une rangée dans un
   * attribut d'un objet de transfert.
   * 
   * @param type
   *          le type de classe
   * @param nomAttribut
   *          le nom d'attribut à remplir
   * @param valeurAttribut
   *          la valeur à spécifier à l'attribut en traitement
   * @parram objetTransfert l'objet de transfert à remplir
   * @throws org.sofiframework.modele.exception.ModeleException
   *           une exception du modèle
   */
  private void convertirEnObjetTransfert(Class type, String nomAttribut,
      Object valeurAttribut, ObjetTransfert objetTransfert)
          throws ModeleException {
    if (type.isAssignableFrom(Row.class)) {
      return;
    }

    try {
      Object valeurConvertie = null;

      if (!type.isAssignableFrom(Row.class)) {
        Class typeObjetTransfert = UtilitaireObjet.getClasse(objetTransfert,
            nomAttribut);
        valeurConvertie = org.sofiframework.modele.adf.convertisseur.Convertisseur
            .convertirEnObjetTransfert(type, typeObjetTransfert, valeurAttribut);
      }

      if (valeurConvertie != null) {
        UtilitaireObjet.setPropriete(objetTransfert, nomAttribut,
            valeurConvertie);
      }
    } catch (InvocationTargetException e) {
      throw new ModeleException(e.getMessage(), e);
    } catch (NoSuchMethodException e) {
      // rien faire;
    } catch (IllegalAccessException e) {
      throw new ModeleException(e.getMessage(), e);
    } catch (IllegalArgumentException e) {
      if (e.getMessage().indexOf("type mismatch") != -1) {
        throw new ModeleException("Erreur de conversion. L'attribut "
            + nomAttribut + " devrait être " + type.getName(), e);
      } else {
        throw new ModeleException(e);
      }
    }
  }

  /**
   * Convertit un type d'attribut provenant d'un objet de transfert vers une
   * rangée d'un objet d'accès aux données.
   * 
   * @param type
   *          le type de classe
   * @param nomAttribut
   *          le nom d'attribut à remplir
   * @param valeurAttribut
   *          la valeur à spécifier à l'attribut en traitement
   * @parram rangee la rangée à traiter.
   * @throws org.sofiframework.modele.exception.ModeleException
   *           une exception du modèle
   */
  private void convertirEnRangee(Class type, String nomAttribut,
      Object valeurAttribut, Row rangee) throws ModeleException {
    try {
      Object valeurConvertie = org.sofiframework.modele.adf.convertisseur.Convertisseur
          .convertirEnRangee(type, valeurAttribut);
      String nomAttributPourRangee = UtilitaireString
          .convertirPremiereLettreEnMajuscule(nomAttribut);
      int indexAttribut = rangee.getAttributeIndexOf(nomAttributPourRangee);
      if (indexAttribut != -1 && rangee.isAttributeUpdateable(indexAttribut)) {
        UtilitaireObjet.setPropriete(rangee, nomAttribut, valeurConvertie);
      }
    } catch (oracle.jbo.ReadOnlyAttrException e) {

    } catch (InvocationTargetException e) {
      throw new ModeleException(e.getTargetException().toString(),
          e.getTargetException());
    } catch (NoSuchMethodException e) {
      // rien faire...
    } catch (IllegalAccessException e) {
      throw new ModeleException(e.getMessage(), e);
    } catch (java.lang.IllegalArgumentException e) {
      if (e.getMessage().indexOf("type mismatch") != -1) {
        throw new ModeleException("Erreur de conversion. L'attribut "
            + nomAttribut + " devrait être " + type.getName(), e);
      } else {
        throw new ModeleException(e);
      }
    }
  }

  /**
   * Retourne une liste d'objet de transfet avec l'aide une vue, d'une
   * définition de classe, d'une condition et des paramètres associés et de
   * l'ordre de tri s'il y a lieu.
   * 
   * @param vue
   *          la vue (View Objet) à utiliser. (Obligatoire)
   * @param classeObjectTransfert
   *          définition d'une classe de type ObjetTranfert. (Obligatoire)
   * @param conditionSql
   *          la condition sql. (Facultatif)
   * @param parametresConditionSql
   *          les paramètres dynamique de la condition sql (Facultatif)
   * @param ordreSql
   *          l'ordre de la liste (Facultatif).
   * @param initialiser
   *          true si vous désirez faire une initialisation de la vue avant
   *          traitement.
   * @return une liste d'objet de transfert.
   * @throws org.sofiframework.modele.exception.ModeleException
   *           une exception du modèle
   */
  protected ArrayList getObjetTransfertListe(BaseViewObjectImpl vue,
      Class classeObjetTransfert, String conditionSql,
      Object[] parametresConditionSql, String ordreSql, boolean initialiser)
          throws ModeleException {
    HashMap classeObjetTransfertMap = new HashMap();
    classeObjetTransfertMap.put("", classeObjetTransfert);

    return this.getObjetTransfertListe(vue, classeObjetTransfertMap,
        conditionSql, parametresConditionSql, ordreSql, initialiser);
  }

  protected synchronized ArrayList getObjetTransfertListe(
      BaseViewObjectImpl vue, HashMap classeRangeeClasseOT,
      String conditionSql, Object[] parametresConditionSql, String ordreSql,
      boolean initialiser) throws ModeleException {
    // Si la vue n'est instancié lancé une Runtime Exception.
    if (vue == null) {
      throw new SOFIException(
          "Le View Objet demandé n'est pas associé au Application Module");
    }

    if (initialiser) {
      vue.initialiser();
    }

    if (conditionSql != null) {
      StringBuffer whereClauseBuffer = null;

      whereClauseBuffer = new StringBuffer();

      if (vue.getWhereClause() != null) {
        whereClauseBuffer.append(vue.getWhereClause());
      }

      whereClauseBuffer.append(" ");
      whereClauseBuffer.append(conditionSql);

      vue.setWhereClause(whereClauseBuffer.toString());
    }

    if (parametresConditionSql != null) {
      vue.setWhereClauseParams(parametresConditionSql);
    }

    if (ordreSql != null) {
      vue.setOrderByClause(ordreSql);
    }

    return this.getObjetTransfertListe(vue, classeRangeeClasseOT);
  }

  /**
   * Retourne une liste d'objet de transfet avec l'aide une vue, d'une
   * définition de classe, d'une condition et des paramètres associés et de
   * l'ordre de tri s'il y a lieu.
   * 
   * @param vue
   *          la vue (View Objet) à utiliser. (Obligatoire)
   * @param classeObjectTransfert
   *          définition d'une classe de type ObjetTranfert. (Obligatoire)
   * @param conditionSql
   *          la condition sql. (Facultatif)
   * @param parametresConditionSql
   *          les paramètres dynamique de la condition sql (Facultatif)
   * @param ordreSql
   *          l'ordre de la liste (Facultatif).
   * @return une liste d'objet de transfert.
   * @throws org.sofiframework.modele.exception.ModeleException
   *           une exception du modèle
   */
  protected ArrayList getObjetTransfertListe(BaseViewObjectImpl vue,
      Class classeObjectTransfert, String conditionSql,
      Object[] parametresConditionSql, String ordreSql) throws ModeleException {
    return getObjetTransfertListe(vue, classeObjectTransfert, conditionSql,
        parametresConditionSql, ordreSql, true);
  }

  /**
   * Recherche une ligne dans une vue à partir de sa clé complexe. Suivant la
   * recherche contruit un objet de transfert et le popule.
   * <p>
   * 
   * @param vue
   *          Vue dans lequel effectuer la recherche
   * @param cle
   *          Clé permettant de retrouver la ligne de la vue.
   * @param classeObjetTransfert
   *          Classe de l'objet de transfert a créer.
   * @return Objet de transfert trouvé et contruit.
   * @throws org.sofiframework.modele.exception.ModeleException
   *           une exception du modèle
   * @throws org.sofiframework.modele.exception.ObjetTransfertNotFoundException
   *           objet de transfert non trouvé
   */
  protected synchronized ObjetTransfert getObjetTransfert(
      BaseViewObjectImpl vue, Object[] cle, Class classeObjetTransfert)
          throws ModeleException, ObjetTransfertNotFoundException {
    vue.initialiser();

    // Si la vue n'est instancié lancé une Runtime Exception.
    if (vue == null) {
      throw new SOFIException(
          "Le View Objet demandé n'est pas associé au Application Module");
    }

    Row[] lignes = vue.rechercherParCleMultiple(cle);

    if ((lignes != null) && (lignes.length > 0)) {
      ObjetTransfert o;

      try {
        o = (ObjetTransfert) classeObjetTransfert.newInstance();
      } catch (InstantiationException e) {
        throw new ModeleException(e);
      } catch (IllegalAccessException e) {
        throw new ModeleException(e);
      }

      this.populerObjetTransfert(lignes[0], o);

      return o;
    } else {
      throw new ObjetTransfertNotFoundException("Objet de transfert non trouvé");
    }
  }

  /**
   * Retourne un objet de transfet avec l'aide une vue, d'une définition de
   * classe, d'une condition et des paramètres associés et de l'ordre de tri
   * s'il y a lieu.
   * 
   * @param vue
   *          la vue (View Objet) à utiliser. (Obligatoire)
   * @param classeObjectTransfert
   *          définition d'une classe de type ObjetTranfert. (Obligatoire)
   * @param conditionSql
   *          la condition sql. (Facultatif)
   * @param parametresConditionSql
   *          les paramètres dynamique de la condition sql (Facultatif)
   * @param ordreSql
   *          l'ordre de la liste (Facultatif).
   * @return l'objet de transfert générer depuis une vue (View Object).
   * @throws org.sofiframework.modele.exception.ModeleException
   *           une exception du modèle
   */
  protected ObjetTransfert getObjetTransfert(BaseViewObjectImpl vue,
      Class classeObjetTransfert, String conditionSql,
      Object[] parametresConditionSql, String ordreSql) throws ModeleException,
      ObjetTransfertNotFoundException {
    HashMap classeObjetTransfertMap = new HashMap();
    classeObjetTransfertMap.put("", classeObjetTransfert);

    return this.getObjetTransfert(vue, classeObjetTransfertMap, conditionSql,
        parametresConditionSql, ordreSql);
  }

  /**
   * Retourne un objet de transfet avec l'aide une vue, d'une définition de
   * classe, d'une condition et des paramètres associés et de l'ordre de tri
   * s'il y a lieu.
   * <p/>
   * Cette méthode est utilisée lorsque l'on accède à un accès de données
   * polymorphique (retourne plusieurs type de rangées).
   * 
   * @param vue
   *          la vue (View Objet) à utiliser. (Obligatoire)
   * @param classeRangeeClasseObjetTransfert
   *          Classes objet de transfert qu sont associés à chaque type de
   *          rangée (Row).
   * @param conditionSql
   *          la condition sql. (Facultatif)
   * @param parametresConditionSql
   *          les paramètres dynamique de la condition sql (Facultatif)
   * @param ordreSql
   *          l'ordre de la liste (Facultatif).
   * @return l'objet de transfert générer depuis une vue (View Object).
   * @throws org.sofiframework.modele.exception.ModeleException
   *           une exception du modèle
   */
  protected synchronized ObjetTransfert getObjetTransfert(
      BaseViewObjectImpl vue, HashMap classeRangeeClasseObjetTransfert,
      String conditionSql, Object[] parametresConditionSql, String ordreSql)
          throws ModeleException, ObjetTransfertNotFoundException {
    // Si la vue n'est instancié lancé une Runtime Exception.
    if (vue == null) {
      throw new SOFIException(
          "Le View Objet demandé n'est pas associé au Application Module");
    }

    vue.initialiser();
    vue.setWhereClause(conditionSql);
    vue.setWhereClauseParams(parametresConditionSql);

    if (ordreSql != null) {
      vue.setOrderByClause(ordreSql);
    }

    if (vue.hasNext()) {
      try {
        Row rangee = vue.next();
        Class classeRangeeClasseObjetTransfertSpecifique = vue
            .fixerClasseObjetTransertSpecifique(rangee);

        if (classeRangeeClasseObjetTransfertSpecifique != null) {
          classeRangeeClasseObjetTransfert = new HashMap();
          classeRangeeClasseObjetTransfert.put("",
              classeRangeeClasseObjetTransfertSpecifique);
        }

        ObjetTransfert objetTransfert = (ObjetTransfert) getClasseObjetTransfert(
            classeRangeeClasseObjetTransfert, rangee).newInstance();
        this.populerObjetTransfert(rangee, objetTransfert);

        vue.traitementApresPopulationObjetTransfert(rangee, objetTransfert);
        return objetTransfert;
      } catch (Exception e) {
        throw new ModeleException(e.getMessage(), e);
      }
    } else {
      throw new ObjetTransfertNotFoundException("Objet de transfert non trouvé");
    }
  }

  /**
   * Détermine quel classe d'objet de transfert correspond à la rangée
   * spécifiée.
   * 
   * @return Classe de l'objet de tranfert à créer pour la classe de rangée.
   * @param rangee
   *          Objet Row obtenu d'un accès de donées
   * @param classeRangeeClasseObjetTransfert
   *          Classes objet de transfert qu sont associés à chaque type de
   *          rangée (Row).
   */
  private Class getClasseObjetTransfert(
      HashMap classeRangeeClasseObjetTransfert, Row rangee) {
    Class classeOT = null;

    if (classeRangeeClasseObjetTransfert.size() == 1) {
      classeOT = (Class) classeRangeeClasseObjetTransfert.values().iterator()
          .next();
    } else {
      classeOT = (Class) classeRangeeClasseObjetTransfert
          .get(rangee.getClass());
    }

    return classeOT;
  }

  /**
   * Obtenir la liste des objets de transfert. Les éléments retournés sont
   * délimités par les propriétés de la liste navigation.
   * 
   * @param vue
   *          Vue qui fournit les éléments de données à populer
   * @param classeObjectTransfert
   *          Classe de l'objet de transfert a produire
   * @param liste
   *          liste de navigation
   * @param conditionSql
   *          critère de requête supplémentaires a appliquer à la vue
   * @param parametresConditionSql
   *          paramètres rquis par la vue et sa clause where supplémentaire
   * @param ordreSql
   *          ordre de tri à utiliser
   * @param initialiser
   *          true si vous désirez faire une initialisation de la vue avant
   *          traitement.
   * @return une liste de navigation populée avec les objets de transfert.
   * @throws org.sofiframework.modele.exception.ModeleException
   *           une exception du modèle
   */
  protected ListeNavigation getObjetTransfertListeNavigation(
      BaseViewObjectImpl vue, Class classeObjectTransfert,
      ListeNavigation liste, String conditionSql,
      Object[] parametresConditionSql, String ordreSql, boolean initialiser)
          throws ModeleException {
    return getObjetTransfertListeNavigation(vue, classeObjectTransfert, liste,
        conditionSql, parametresConditionSql, ordreSql, initialiser, false);
  }

  /**
   * Obtenir la liste des objets de transfert. Les éléments retournés sont
   * délimités par les propriétés de la liste navigation.
   * 
   * @param vue
   *          Vue qui fournit les éléments de données à populer
   * @param classeObjectTransfert
   *          Classe de l'objet de transfert a produire
   * @param liste
   *          liste de navigation
   * @param conditionSql
   *          critère de requête supplémentaires a appliquer à la vue
   * @param parametresConditionSql
   *          paramètres rquis par la vue et sa clause where supplémentaire
   * @param ordreSql
   *          ordre de tri à utiliser
   * @param initialiser
   *          true si vous désirez faire une initialisation de la vue avant
   *          traitement.
   * @param appliquerFiltre
   *          true si vous désirez appliquer le filtre de la liste de
   *          navigation.
   * @return une liste de navigation populée avec les objets de transfert.
   * @throws org.sofiframework.modele.exception.ModeleException
   *           une exception du modèle
   */
  protected synchronized ListeNavigation getObjetTransfertListeNavigation(
      BaseViewObjectImpl vue, Class classeObjectTransfert,
      ListeNavigation liste, String conditionSql,
      Object[] parametresConditionSql, String ordreSql, boolean initialiser,
      boolean appliquerFiltre) throws ModeleException {
    if (initialiser) {
      vue.initialiser();
    }

    vue.setWhereClause(conditionSql);
    vue.setWhereClauseParams(parametresConditionSql);

    if (ordreSql != null) {
      vue.setOrderByClause(ordreSql);
    }

    if (appliquerFiltre) {
      if ((liste.getFiltre() != null) && (vue.getViewCriteria() == null)) {
        vue.appliquerFiltre(liste.getFiltre(), false);
      }
    }

    liste.setListe(this.getObjetTransfertListe(vue,
        vue.appliquerListeNavigation(liste), classeObjectTransfert));

    return liste;
  }

  /**
   * Obtenir la liste des objets de transfert. Les éléments retournés sont
   * délimités par les propriétés de la liste navigation.
   * 
   * @param vue
   *          Vue qui fournit les éléments de données à populer
   * @param classeObjectTransfert
   *          Classe de l'objet de transfert a produire
   * @param liste
   *          liste de navigation
   * @param conditionSql
   *          critère de requête supplémentaires a appliquer à la vue
   * @param parametresConditionSql
   *          paramètres rquis par la vue et sa clause where supplémentaire
   * @param ordreSql
   *          ordre de tri à utiliser
   * @return une liste de navigation populée avec les objets de transfert.
   * @throws org.sofiframework.modele.exception.ModeleException
   *           une exception du modèle
   */
  protected ListeNavigation getObjetTransfertListeNavigation(
      BaseViewObjectImpl vue, Class classeObjectTransfert,
      ListeNavigation liste, String conditionSql,
      Object[] parametresConditionSql, String ordreSql) throws ModeleException {
    return getObjetTransfertListeNavigation(vue, classeObjectTransfert, liste,
        conditionSql, parametresConditionSql, ordreSql, true);
  }

  /**
   * Obtenir la liste des objets de transfert. Les éléments retournés sont
   * délimités par les propriétés de la liste navigation. Tous les éléments de
   * la vue sont retournés. Aucune restriction supplémentaire n'est appliquée
   * n'est appliqués à la vue.
   * 
   * @param vue
   *          Vue qui fournit les éléments de données à populer
   * @param classeObjectTransfert
   *          Classe de l'objet de transfert a produire
   * @param liste
   *          liste de navigation
   * @return ListeNavigation une liste de navigation populée avec les objets de
   *         transfert.
   * @throws org.sofiframework.modele.exception.ModeleException
   *           une exception du modèle
   */
  protected synchronized ListeNavigation getObjetTransfertListeNavigation(
      BaseViewObjectImpl vue, Class classeObjectTransfert, ListeNavigation liste)
          throws ModeleException {
    if ((liste.getObjetFiltre() != null) && (vue.getViewCriteria() == null)) {
      vue.initialiser();
      vue.appliquerFiltre(liste.getObjetFiltre());
    }

    liste.setListe(this.getObjetTransfertListe(vue,
        vue.appliquerListeNavigation(liste), classeObjectTransfert));

    return liste;
  }

  /**
   * Obtenir une liste de navigation populée d'objet s de transferts.
   * <p/>
   * Cette méthode est utilisée lorsque l'on accède à un accès de données
   * polymorphique (retourne plusieurs type de rangées).
   * 
   * @throws org.sofiframework.modele.exception.ModeleException
   *           Erreur survenu lors de l'accès au modèle
   * @return Liste de navigation populée
   * @param liste
   *          Liste de navigation qui doit être populé par l'accès de données
   * @param classeRangeeClasseObjetTransfert
   *          Classes objet de transfert qu sont associés à chaque type de
   *          rangée (Row).
   * @param vue
   *          Accès de données en cours de traitement
   */
  protected synchronized ListeNavigation getObjetTransfertListeNavigation(
      BaseViewObjectImpl vue, HashMap classeRangeeClasseObjetTransfert,
      ListeNavigation liste) throws ModeleException {
    if ((liste.getObjetFiltre() != null) && (vue.getViewCriteria() == null)) {
      vue.initialiser();
      vue.appliquerFiltre(liste.getObjetFiltre());
    }

    liste.setListe(this.getObjetTransfertListe(vue,
        vue.appliquerListeNavigation(liste), classeRangeeClasseObjetTransfert));

    return liste;
  }

  /**
   * Obtenir la liste des objets de transfert qui sont retournés après filtre.
   * Les données retournés sont limités par la liste de navigation passée en
   * paramètre.
   * 
   * @param vue
   *          Vue qui fournit les éléments de données à populer
   * @param classeObjectTransfert
   *          Classe de l'objet de transfert a produire
   * @param liste
   *          liste de navigation
   * @param filtre
   *          Objet qui regroupe un nombre de critères de recherche
   * @param initialiser
   *          true si vous désirez faire une initialisation de la vue avant
   *          traitement.
   * @return liste de navigation qui est populé avec les object de transfert
   *         retournés par la vue.
   * @throws org.sofiframework.modele.exception.ModeleException
   *           une exception du modèle
   */
  protected synchronized ListeNavigation getObjetTransfertListeNavigation(
      BaseViewObjectImpl vue, Class classeObjectTransfert,
      ListeNavigation liste, FiltreVue filtre, boolean initialiser)
          throws ModeleException {
    if (initialiser) {
      vue.initialiser();
    }

    try {
      filtre.appliquer(vue);
    } catch (Exception ex) {
      log.error("Erreur getObjetTransfertListeNavigation", ex);
    }

    liste.setListe(this.getObjetTransfertListe(vue,
        vue.appliquerListeNavigation(liste), classeObjectTransfert));

    return liste;
  }

  /**
   * Obtenir la liste des objets de transfert qui sont retournés après filtre.
   * Les données retournés sont limités par la liste de navigation passée en
   * paramètre.
   * 
   * @param vue
   *          Vue qui fournit les éléments de données à populer
   * @param classeObjectTransfert
   *          Classe de l'objet de transfert a produire
   * @param liste
   *          liste de navigation
   * @param filtre
   *          Objet qui regroupe un nombre de critères de recherche
   * @return liste de navigation qui est populé avec les object de transfert
   *         retournés par la vue.
   * @throws org.sofiframework.modele.exception.ModeleException
   *           une exception du modèle
   */
  protected ListeNavigation getObjetTransfertListeNavigation(
      BaseViewObjectImpl vue, Class classeObjectTransfert,
      ListeNavigation liste, FiltreVue filtre) throws ModeleException {
    return getObjetTransfertListeNavigation(vue, classeObjectTransfert, liste,
        filtre, true);
  }

  /**
   * Obtenir une liste d'objets de transfert à partir d'un tableau de lignes
   * d'une vue.
   * 
   * @param lignes
   *          les lignes qui doivent populer les Objets de transferts de la
   *          liste
   * @param classeObjetTransfert
   *          la classe de l'objet de transfert qui doit constituer les éléments
   *          de la liste.
   * @return Liste d'objets de transfert contenant les informations des lignes
   *         passés en paramètre.
   * @throws org.sofiframework.modele.exception.ModeleException
   *           une exception du modèle
   */
  protected ArrayList getObjetTransfertListe(BaseViewObjectImpl vue,
      Row[] lignes, Class classeObjetTransfert) throws ModeleException {
    HashMap classes = new HashMap();
    classes.put("", classeObjetTransfert);

    return getObjetTransfertListe(vue, lignes, classes);
  }

  /**
   * Obtenir une liste d'objets de transfert à partir d'un tableau de lignes
   * d'une vue.
   * <p/>
   * Cette méthode est utilisée lorsque l'on accède à un accès de données
   * polymorphique (retourne plusieurs type de rangées).
   * 
   * @param lignes
   *          les lignes qui doivent populer les Objets de transferts de la
   *          liste
   * @param classeRangeeClasseObjetTransfert
   *          Classes objet de transfert qu sont associés à chaque type de
   *          rangée (Row).
   * @return Liste d'objets de transfert contenant les informations des lignes
   *         passés en paramètre.
   * @throws org.sofiframework.modele.exception.ModeleException
   *           une exception du modèle
   */
  protected ArrayList getObjetTransfertListe(BaseViewObjectImpl vue,
      Row[] lignes, HashMap classeRangeeClasseObjetTranfert)
          throws ModeleException {
    ArrayList liste = null;

    try {
      if ((lignes != null) && (lignes.length > 0)) {
        liste = new ArrayList();

        // Populer la liste des documents
        for (int i = 0; i < lignes.length; i++) {
          if (lignes[i] != null) {
            ObjetTransfert o = this.populerObjetTransfert(
                lignes[i],
                (ObjetTransfert) getClasseObjetTransfert(
                    classeRangeeClasseObjetTranfert, lignes[i]).newInstance());
            vue.traitementApresPopulationObjetTransfert(lignes[i], o);
            liste.add(o);
          }
        }
      }

      return liste;
    } catch (ModeleException e) {
      throw e;
    } catch (Exception ex) {
      throw new ModeleException(ex);
    }
  }

  /**
   * Obtenir une liste d'objets de transfert à partir d'un iterateur de lignes
   * d'une vue.
   * 
   * @param iterateurLigne
   *          Iterateur sur les lignes de la vue
   * @param classeObjetTransfert
   *          classe de l'objet de transfert qui doit constituer les éléments de
   *          la liste.
   * @return Liste d'objets de transfert contenant les informations des lignes
   *         passés en paramètre.
   * @throws org.sofiframework.modele.exception.ModeleException
   *           une exception du modèle
   */
  public synchronized ArrayList getObjetTransfertListe(
      RowIterator iterateurLigne, Class classeObjetTransfert)
          throws ModeleException {
    if (iterateurLigne instanceof BaseViewObjectImpl) {
      ((BaseViewObjectImpl) iterateurLigne).initialiser();
    }

    HashMap classeObjectTransfertMap = new HashMap();
    classeObjectTransfertMap.put("", classeObjetTransfert);

    return this
        .getObjetTransfertListe(iterateurLigne, classeObjectTransfertMap);
  }

  /**
   * Obtenir une liste d'objets de transfert à partir d'un iterateur de lignes
   * d'une vue.
   * 
   * @param initialiser
   *          permet d'initialiser la cache et les paramètres dynamiques
   *          (condition sql) associé la vue.
   * @param vue
   *          Iterateur sur les lignes de la vue
   * @param classeObjetTransfert
   *          classe de l'objet de transfert qui doit constituer les éléments de
   *          la liste.
   * @return Liste d'objets de transfert contenant les informations des lignes
   *         passés en paramètre.
   * @throws org.sofiframework.modele.exception.ModeleException
   *           une exception du modèle
   */
  protected synchronized ArrayList getObjetTransfertListe(
      BaseViewObjectImpl vue, Class classeObjetTransfert, boolean initialiser)
          throws ModeleException {
    if (initialiser) {
      vue.initialiser();
    }

    HashMap classeObjectTransfertMap = new HashMap();
    classeObjectTransfertMap.put("", classeObjetTransfert);

    return this.getObjetTransfertListe(vue, classeObjectTransfertMap);
  }

  /**
   * Obtenir une liste d'objets de transfert à partir d'un iterateur de lignes
   * d'une vue.
   * 
   * @param iterateurLigne
   *          Iterateur sur les lignes de la vue
   * @param classeRangeeClasseObjetTransfert
   *          Classes objet de transfert qu sont associés à chaque type de
   *          rangée (Row).
   * @return Liste d'objets de transfert contenant les informations des lignes
   *         passés en paramètre.
   * @throws org.sofiframework.modele.exception.ModeleException
   *           une exception du modèle
   */
  public ArrayList getObjetTransfertListe(RowIterator iterateurLigne,
      HashMap classeRangeeClasseObjetTransfert) throws ModeleException {
    ArrayList liste = null;

    try {
      if (iterateurLigne.hasNext()) {
        liste = new ArrayList();

        // Populer la liste des documents
        while (iterateurLigne.hasNext()) {
          Row rangee = iterateurLigne.next();
          ObjetTransfert o = this.populerObjetTransfert(
              rangee,
              (ObjetTransfert) getClasseObjetTransfert(
                  classeRangeeClasseObjetTransfert, rangee).newInstance());

          if (iterateurLigne instanceof BaseViewObjectImpl) {
            ((BaseViewObjectImpl) iterateurLigne)
            .traitementApresPopulationObjetTransfert(rangee, o);
          }

          if (o != null) {
            liste.add(o);
          }
        }
      }
    } catch (InstantiationException e) {
      throw new ModeleException(e);
    } catch (IllegalAccessException e) {
      throw new ModeleException(e);
    }

    return liste;
  }

  /**
   * Obtenir la liste des objets de transfert qui correspondent au critères
   * spécifiés par un objet filtre.
   * 
   * @throws org.sofiframework.modele.exception.ModeleException
   *           Exception survenu lors de l'accès au modèle
   * @return Liste des objets de transfert
   * @param filtre
   *          Objet filtre qui décrit les critères de recherche sur l'accès de
   *          données
   * @param classeRangeeClasseObjetTransfert
   *          Correspondance entre les classes d'objet de transfert et les
   *          classes de rangées de vue.
   * @param vue
   *          Accès de données dont on doit obtenir une liste d'objet
   */
  public ArrayList getObjetTransfertListe(BaseViewObjectImpl vue,
      HashMap classeRangeeClasseObjetTransfert, Filtre filtre)
          throws ModeleException {
    // Si la vue n'est instancié lancé une Runtime Exception.
    if (vue == null) {
      throw new SOFIException(
          "Le View Objet demandé n'est pas associé au Application Module");
    }

    vue.appliquerFiltre(filtre);

    return this.getObjetTransfertListe(vue, classeRangeeClasseObjetTransfert);
  }

  /**
   * Obtenir la liste des objets de transfert qui correspondent au critères
   * spécifiés par un objet filtre.
   * 
   * @throws org.sofiframework.modele.exception.ModeleException
   *           Exception survenu lors de l'accès au modèle
   * @return Liste des objets de transfert
   * @param filtre
   *          Objet filtre qui décrit les critères de recherche sur l'accès de
   *          données
   * @param classeObjetTransfert
   *          classe de l'objet de transfert qui constituera la liste
   * @param vue
   *          Accès de données dont on doit obtenir une liste d'objet
   */
  public ArrayList getObjetTransfertListe(BaseViewObjectImpl vue,
      Class classeObjetTransfert, Filtre filtre) throws ModeleException {
    HashMap classeObjetTransfertMap = new HashMap();
    classeObjetTransfertMap.put("", classeObjetTransfert);

    return this.getObjetTransfertListe(vue, classeObjetTransfertMap, filtre);
  }

  /**
   * Retourne la connexion de la base de données présentement en utilisation.
   * 
   * @return la connexion BD
   * @throws java.sql.SQLException
   */
  @Override
  public Connection getConnexionBD() throws SQLException {
    Statement stmt = this.getDBTransaction().createStatement(0);

    try {
      return stmt.getConnection();
    } finally {
      close(stmt);
    }
  }

  /**
   * Permet de faire une mise à jour de la transaction sans commiter. Il fait
   * donc un postChanges. Si exception, ModeleException est lancé.
   * 
   * @throws org.sofiframework.modele.exception.ModeleException
   */
  protected void postChanges() throws ModeleException {
    try {
      getDBTransaction().validate();
      getDBTransaction().postChanges();
    } catch (RowInconsistentException e) {
      String erreurComplete = e.getMessage();

      if ((e.getDetails() != null) && (e.getDetails().length > 0)) {
        erreurComplete += e.getDetails()[0];
      }

      throw new ConcurrenceException(erreurComplete, e, e.getErrorCode(),
          e.getDetails());
    } catch (JboException e) {
      String erreurComplete = e.getMessage();

      if ((e.getDetails() != null) && (e.getDetails().length > 0)) {
        erreurComplete += e.getDetails()[0];
      }

      throw new ModeleException(erreurComplete, e, e.getErrorCode(),
          e.getDetails());
    }
  }

  @Override
  public void initialiserTransaction() {
  }

  /**
   * Permet de faire une mise à jour complète de la transaction. Il fait donc un
   * commit.
   */
  @Override
  public void commit() {
    try {
      getDBTransaction().commit();
    } catch (JboException e) {
      throw new ModeleException(e);
    }
  }

  /**
   * Annule les mise à jour faites au cours de la transactioné
   */
  @Override
  public void rollback() {
    try {
      getDBTransaction().rollback();
    } catch (JboException e) {
      throw new ModeleException(e);
    }
  }

  @Override
  public void libererTransaction() {
  }

  /**
   * Ré-écriture d'une méthode de base de Application Module qui permet de
   * charger les services imbriqués (Nested) sur demande seulement.
   * 
   * @return true afin de permettre le chargement de service imbriqué sur
   *         demande seulement.
   */
  /**
   * public boolean isLoadComponentsLazily() { return true; }
   **/

  /**
   * Est-ce que l'exception lancé est une contrainte d'intégrité concernant la
   * clé d'unicité ?
   * 
   * @param detailException
   *          l'exception du modele
   * @return true si l'exception lancé est une contrainte d'intégrité est
   *         concernant la clé d'unicité.
   */
  protected boolean isExceptionCleUnique(ModeleException detailException) {
    if (detailException.getDetail() == null) {
      return false;
    }

    return org.sofiframework.modele.adf.exception.UtilitaireExceptionOracle
        .isCleUniqueException(detailException.getDetail());
  }

  /**
   * Retourne le message d'erreur bd
   * 
   * @param detailException
   *          le détail de l'exception.
   * @return le message d'erreur bd.
   */
  protected String getMessageErreurBD(ModeleException detailException) {
    if (detailException.getDetail() == null) {
      return detailException.getMessage();
    }

    return org.sofiframework.modele.adf.exception.UtilitaireExceptionOracle
        .getMessageErreurBD(detailException.getDetail());
  }

  /**
   * Retourne l'utilisateur pour le rendre disponible dans tous les services.
   * 
   * @return l'utilisateur authentifié
   */
  @Override
  public Utilisateur getUtilisateur() {
    return getUtilisateurEnCours();
  }

  /**
   * Fixer l'utilisateur pour le rendre disponible dans tous les services.
   * 
   * @param utilisateur
   *          l'utilisateur authentifié.
   */
  public void setUtilisateur(Utilisateur utilisateur) {
    if (getDBTransaction().getSession().getUserData() != null) {
      Hashtable session = getDBTransaction().getSession().getUserData();
      session.put(Constantes.UTILISATEUR, utilisateur);
    }
  }

  /**
   * Préparation de la session utilisateur pour le service.
   * <p>
   * Vous devez hériter et implanter cette méthode.
   * <p>
   * Voici un exemple: <code>
   * if (getUtilisateur() != null) {
   *  StringBuffer appContext = new StringBuffer();
   *  appContext.append("BEGIN ");
   *  appContext.append(GestionSecurite.getInstance().getProcedureContexteBD());
   *  appContext.append(" (");
   *  appContext.append("'");
   *  appContext.append(getUtilisateur().getCodeUtilisateur());
   *  appContext.append("'");
   *  appContext.append("); END;");
   * 
   *  CallableStatement st = null;
   * 
   *  try {
   *    st = getDBTransaction().createCallableStatement(appContext.toString(), 0);
   *    st.execute();
   *  } catch (SQLException e) {
   *    throw new JboException(e);
   *  } finally {
   *    close(st);
   *    } catch (SQLException e2) {
   *  }
   * }
   * </code>
   * 
   * @param _session
   *          la session du modèle.
   */
  @Override
  public void prepareSession(Session _session) {
    super.prepareSession(_session);
  }

  /**
   * Simplife les appels à un procédure stocké BD avec des paramètres associé.
   * 
   * Utiliser cette signature sans les types lorsque vos paramètres ne peuvent
   * pas être nuls.
   * 
   * @param procedure
   *          la procédure stocké à exécuté.
   * @param variableAssocie
   *          le tableau d'objet associé à la procédure. Mettre null si aucun
   *          paramètre.
   * @throws ModeleException
   *           l'exception résultante de l'éxécution de la procédure.
   * @deprecated Utiliser plutôt la version avec les types associés passés en
   *             paramètre pour une meilleure gestion des valeurs nulles.
   */
  @Deprecated
  protected void executePLSQL(String procedure, Object[] variableAssocie)
      throws ModeleException {
    CallableStatement st = null;

    StringBuffer procedureComplete = new StringBuffer();

    if (procedure.toLowerCase().indexOf("begin") < 0) {
      procedureComplete.append("begin ");
      procedureComplete.append(procedure);
      procedureComplete.append(" end;");
    } else {
      procedureComplete.append(procedure);
    }

    try {
      if (variableAssocie != null) {
        st = getDBTransaction().createCallableStatement(
            procedureComplete.toString(), 0);

        if (variableAssocie != null) {
          for (int z = 0; z < variableAssocie.length; z++) {
            st.setObject(z + 1, variableAssocie[z]);
          }
        }

        st.executeUpdate();
      } else {
        getDBTransaction().executeCommand(procedureComplete.toString());
      }
    } catch (SQLException e) {
      throw new ModeleException(e);
    } finally {
      close(st);
    }
  }

  /**
   * Simplife les appels à un procédure stocké BD avec des paramètres associé.
   * 
   * Utiliser cette signature avec les types lorsque certains de vos paramètres
   * peuvent être nuls.
   * 
   * @param procedure
   *          la procédure stocké à exécuté.
   * @param variableAssocie
   *          le tableau d'objet associé à la procédure. Mettre null si aucun
   *          paramètre.
   * @param typesAssocies
   *          Un tableau contenant les types des paramètres, voir ConstantesSQL
   *          pour les types disponibles.
   * @throws ModeleException
   *           l'exception résultante de l'éxécution de la procédure.
   * @deprecated Depuis SOFI 2.0, utilisez executerProcedurePLSQL(..)
   */
  @Deprecated
  protected void executePLSQL(String procedure, Object[] variableAssocie,
      int[] typesAssocies) throws ModeleException {
    CallableStatement st = null;

    StringBuffer procedureComplete = new StringBuffer();

    if (procedure.toLowerCase().indexOf("begin") < 0) {
      procedureComplete.append("begin ");
      procedureComplete.append(procedure);
      procedureComplete.append(" end;");
    } else {
      procedureComplete.append(procedure);
    }

    try {
      if (variableAssocie != null) {
        st = getDBTransaction().createCallableStatement(
            procedureComplete.toString(), 0);

        if (variableAssocie != null) {
          for (int z = 0; z < variableAssocie.length; z++) {
            if (variableAssocie[z] == null) {
              st.setNull(z + 1, typesAssocies[z]);
            } else {
              st.setObject(z + 1, variableAssocie[z]);
            }
          }
        }

        st.executeUpdate();
      } else {
        getDBTransaction().executeCommand(procedureComplete.toString());
      }
    } catch (SQLException e) {
      throw new ModeleException(e);
    } finally {
      close(st);
    }
  }

  /**
   * Simplife les appels à un procédure stocké BD avec des paramètres associé.
   * 
   * Utiliser cette signature avec les types lorsque certains de vos paramètres
   * peuvent être nuls.
   * 
   * @param procedure
   *          la procédure stocké à exécuté.
   * @param variableAssocie
   *          le tableau d'objet associé à la procédure. Mettre null si aucun
   *          paramètre.
   * @param typesAssocies
   *          Un tableau contenant les types des paramètres, voir ConstantesSQL
   *          pour les types disponibles.
   * @throws ModeleException
   *           l'exception résultante de l'éxécution de la procédure.
   */
  protected void executerProcedurePLSQL(String procedure,
      Object[] variableAssocie, int[] typesAssocies) throws ModeleException {
    CallableStatement st = null;

    StringBuffer procedureComplete = new StringBuffer();

    if (procedure.toLowerCase().indexOf("begin") < 0) {
      procedureComplete.append("begin ");
      procedureComplete.append(procedure);
      procedureComplete.append(" end;");
    } else {
      procedureComplete.append(procedure);
    }

    try {
      if (variableAssocie != null) {
        st = getDBTransaction().createCallableStatement(
            procedureComplete.toString(), 0);

        if (variableAssocie != null) {
          for (int z = 0; z < variableAssocie.length; z++) {
            if (variableAssocie[z] == null) {
              st.setNull(z + 1, typesAssocies[z]);
            } else {
              st.setObject(z + 1, variableAssocie[z]);
            }
          }
        }

        st.executeUpdate();
      } else {
        getDBTransaction().executeCommand(procedureComplete.toString());
      }
    } catch (SQLException e) {
      throw new ModeleException(e);
    } finally {
      close(st);
    }
  }

  /**
   * Simplifie l'appel d'une procédure stocké BD avec des paramètres associé.
   * 
   * Utiliser cette signature sans les types lorsque vos paramètres ne peuvent
   * pas être nuls.
   * 
   * Vous pouvez utiliser les types de constantes NUMBER, DATE, et VARCHAR2 de
   * la classe ConstantesSQL pour le type de retour, sinon le type JDBC de
   * retour sera java.sql.Types.
   * 
   * @param sqlReturnType
   *          le type de donnée JDBC de retour, voir ConstantesSQL pour les
   *          types disponibles.
   * @param fonction
   *          la fonction stocké BD
   * @param variableAssocies
   *          Un tableau d'objet de paramètre associé à la fonction.
   * @return Retourne la valeur de l'objet retour.
   * @throws ModeleException
   *           l'exception résultante de l'éxécution de la procédure.
   * @deprecated Depuis SOFI 2.0, utilisez executerFonctionPLSQL(...)
   */
  @Deprecated
  protected Object executeProcedurePLSQL(int typeSQLRetour, String fonction,
      Object[] variableAssocies) throws ModeleException {
    CallableStatement st = null;

    StringBuffer fonctionComplete = new StringBuffer();

    if (fonction.toLowerCase().indexOf("begin") < 0) {
      fonctionComplete.append("begin ");
      fonctionComplete.append(fonction);
      fonctionComplete.append(" end;");
    } else {
      fonctionComplete.append(fonction);
    }

    try {
      st = getDBTransaction().createCallableStatement(
          fonctionComplete.toString(), 0);
      st.registerOutParameter(1, typeSQLRetour);

      if (variableAssocies != null) {
        for (int z = 0; z < variableAssocies.length; z++) {
          st.setObject(z + 2, variableAssocies[z]);
        }
      }

      st.executeUpdate();

      return st.getObject(1);
    } catch (SQLException e) {
      throw new ModeleException(e);
    } finally {
      close(st);
    }
  }

  /**
   * Simplifie l'appel d'une procédure stocké BD avec des paramètres associé.
   * 
   * Utiliser cette signature avec les types lorsque certains de vos paramètres
   * peuvent être nuls.
   * 
   * Vous pouvez utiliser les types de constantes NUMBER, DATE, et VARCHAR2 de
   * la classe ConstantesSQL pour le type de retour, sinon le type JDBC de
   * retour sera java.sql.Types.
   * 
   * Par défaut, le paramètre de sorite est à la position 1 de la procedure
   * tandis que les paramètres sont des parametre d'entrées.
   * 
   * @param sqlReturnType
   *          le type de donnée JDBC de retour, voir ConstantesSQL pour les
   *          types disponibles.
   * @param fonction
   *          la fonction stocké BD
   * @param variableAssocies
   *          Un tableau d'objet de paramètre associé à la fonction.
   * @param typesAssocies
   *          Un tableau contenant les types des paramètres, voir ConstantesSQL
   *          pour les types disponibles.
   * @return Retourne la valeur de l'objet retour.
   * @throws ModeleException
   *           l'exception résultante de l'éxécution de la procédure.
   * @deprecated Depuis SOFI 2.0, utilisez executerFonctionPLSQL(...)
   */
  @Deprecated
  protected Object executeProcedurePLSQL(int typeSQLRetour, String fonction,
      Object[] variableAssocies, int[] typesAssocies) throws ModeleException {
    return UtilitairePLSQL.executerFonctionPLSQL(typeSQLRetour, 1, 2, fonction,
        variableAssocies, typesAssocies, getDBTransaction());
  }

  /**
   * Simplifie l'appel d'une procédure stocké BD avec des paramètres associé.
   * 
   * Utiliser cette signature avec les types lorsque certains de vos paramètres
   * peuvent être nuls.
   * 
   * Vous pouvez utiliser les types de constantes NUMBER, DATE, et VARCHAR2 de
   * la classe ConstantesSQL pour le type de retour, sinon le type JDBC de
   * retour sera java.sql.Types.
   * 
   * Par défaut, le paramètre de sorite est à la position 1 de la procedure
   * tandis que les paramètres sont des parametre d'entrées.
   * 
   * @param sqlReturnType
   *          le type de donnée JDBC de retour, voir ConstantesSQL pour les
   *          types disponibles.
   * @param fonction
   *          la fonction stocké BD
   * @param variableAssocies
   *          Un tableau d'objet de paramètre associé à la fonction.
   * @param typesAssocies
   *          Un tableau contenant les types des paramètres, voir ConstantesSQL
   *          pour les types disponibles.
   * @return Retourne la valeur de l'objet retour.
   * @throws ModeleException
   *           l'exception résultante de l'éxécution de la procédure.
   */
  protected Object executerFonctionPLSQL(int typeSQLRetour, String fonction,
      Object[] variableAssocies, int[] typesAssocies) throws ModeleException {
    return UtilitairePLSQL.executerFonctionPLSQL(typeSQLRetour, 1, 2, fonction,
        variableAssocies, typesAssocies, getDBTransaction());
  }

  /**
   * Simplifie l'appel d'une procédure stocké BD avec des paramètres associé.
   * 
   * Utiliser cette signature avec les types lorsque certains de vos paramètres
   * peuvent être nuls.
   * 
   * Vous pouvez utiliser les types de constantes NUMBER, DATE, et VARCHAR2 de
   * la classe ConstantesSQL pour le type de retour, sinon le type JDBC de
   * retour sera java.sql.Types.
   * 
   * @param typeSQLRetour
   *          Un tableau contenant les types des données JDBC de retour, voir
   *          ConstantesSQL pour les types disponibles.
   * @param fonction
   *          la fonction stocké BD.
   * @param positionPremierParametreSortie
   *          la position du premier paramètre en sortie.
   * @param variableAssocies
   *          Un tableau d'objet de paramètre associé à la fonction.
   * @param typesAssocies
   *          Un tableau contenant les types des paramètres, voir ConstantesSQL
   *          pour les types disponibles.
   * @return Retourne un tableau contenant les valeurs des objets de retour.
   * @throws ModeleException
   *           l'exception résultante de l'éxécution de la procédure.
   * @deprecated Depuis SOFI 2.0, utilisez executerProcedurePLSQL(...)
   */
  @Deprecated
  protected Object[] executePLSQL(int[] typeSQLRetour, String fonction,
      int positionPremierParametreSortie, Object[] variableAssocies,
      int[] typesAssocies) throws ModeleException {
    return UtilitairePLSQL.executerProcedurePLSQL(typeSQLRetour, fonction,
        positionPremierParametreSortie, variableAssocies, typesAssocies,
        getDBTransaction());
  }

  /**
   * Simplifie l'appel d'une procédure stocké BD avec des paramètres associé.
   * 
   * Utiliser cette signature avec les types lorsque certains de vos paramètres
   * peuvent être nuls.
   * 
   * Vous pouvez utiliser les types de constantes NUMBER, DATE, et VARCHAR2 de
   * la classe ConstantesSQL pour le type de retour, sinon le type JDBC de
   * retour sera java.sql.Types.
   * 
   * @param typeSQLRetour
   *          Un tableau contenant les types des données JDBC de retour, voir
   *          ConstantesSQL pour les types disponibles.
   * @param fonction
   *          la fonction stocké BD.
   * @param positionPremierParametreSortie
   *          la position du premier paramètre en sortie.
   * @param variableAssocies
   *          Un tableau d'objet de paramètre associé à la fonction.
   * @param typesAssocies
   *          Un tableau contenant les types des paramètres, voir ConstantesSQL
   *          pour les types disponibles.
   * @return Retourne un tableau contenant les valeurs des objets de retour.
   * @throws ModeleException
   *           l'exception résultante de l'éxécution de la procédure.
   */
  protected Object[] executerProcedurePLSQL(int[] typeSQLRetour,
      String fonction, int positionPremierParametreSortie,
      Object[] variableAssocies, int[] typesAssocies) throws ModeleException {
    return UtilitairePLSQL.executerProcedurePLSQL(typeSQLRetour, fonction,
        positionPremierParametreSortie, variableAssocies, typesAssocies,
        getDBTransaction());
  }

  /**
   * Obtenir un service distant à partir de son nom.
   * 
   * @param nom
   *          Le nom du service recherché.
   * @return Un service distant EJB
   */
  public EJBObject getServiceDistant(String nom) throws Exception {
    return (EJBObject) GestionServiceDistant.getServiceDistant(nom);
  }

  /**
   * Retourne un tableau de valeur avec la valeur d'un objet de transfert
   * contenu dans une liste.
   * 
   * @return le tableau de valeur.
   * @param nomAttributValeur
   *          le nom attribut dont le tableau dont contenir la valeur.
   * @param liste
   *          la liste a traiter.
   */
  protected String[] getTableauValeurPourListe(ArrayList liste,
      String nomAttributValeur) {
    if (liste != null) {
      String[] valeurRetour = new String[liste.size()];
      Iterator iterateur = liste.iterator();
      int i = 0;

      while (iterateur.hasNext()) {
        ObjetTransfert objetTransfert = (ObjetTransfert) iterateur.next();

        try {
          String valeur = (String) UtilitaireObjet.getValeurAttribut(
              objetTransfert, nomAttributValeur);
          valeurRetour[i] = valeur;
          i++;
        } catch (Exception e) {
          throw new SOFIException("Nom attribut introuvable:"
              + nomAttributValeur);
        }
      }

      return valeurRetour;
    } else {
      return null;
    }
  }

  /**
   * Obtenir le service générique qui correspond à l'bjet de transfert spécifié.
   * Si un service spécialisé n'est pas trouvé pour l'objet, on retourne la
   * version générique du framework. Certains paramètre de nomenclature sont
   * utilisés pour rendre plus flexible la structure du modèle. Les paramètes
   * doivent être ajoutés au fichier xml de paraètre du modèle
   * (modele-parametre-sofi.xml).
   * 
   * Voici les paramètres utilisés par cette méthode :
   * 
   * - definitionServiceGenerique - paquetageObjetTransfert - paquetageService -
   * prefixeService - suffixeService
   * 
   * Exemple de nom de classe d'objet de transfert :
   * com.organisation.produit.modele.facette.[paquetageObjetTransfert].Entite
   * 
   * Nom du service générique attendu :
   * com.organisation.produit.modele.facette.[
   * paquetageService].[prefixeService]Entite[suffixeService]
   * 
   * @return Service générique enfant du service en cours
   * 
   * @param classeObjetTransfert
   *          Classe de l'objet de tranfert en cours de traitement. Si il s'agit
   *          d'une unité de traitement polymorphique, spécifier l'objet de
   *          tranfert parent.
   */
  public BaseServiceGenerique getServiceGenerique(Class classeObjetTransfert)
      throws ModeleException {
    BaseServiceGeneriqueImpl service = null;
    boolean serviceTrouve = false;
    boolean renduAObjetTransfert = false;

    String premiereDefinition = org.sofiframework.modele.adf.generique.utilitaire.UtilitaireServiceGenerique
        .genererNomDefinitionService(classeObjetTransfert);
    String premierNom = premiereDefinition.substring(premiereDefinition
        .lastIndexOf(".") + 1);
    Class classeATraiter = classeObjetTransfert;

    /*
     * On recherche jusqua ce que lon trouve le service ou que l'on soit rendu à
     * l'objet de transfert. Si on se rend à l'objet de transfert on utilise le
     * service générique.
     */
    while (!serviceTrouve && !renduAObjetTransfert) {
      // Nom de définition de service correspondant au nom de classe
      String nomDefinition = org.sofiframework.modele.adf.generique.utilitaire.UtilitaireServiceGenerique
          .genererNomDefinitionService(classeATraiter);

      // Nom utilisé pour l'application module est la derniere partie du nom de
      // la définition.
      String nom = nomDefinition.substring(nomDefinition.lastIndexOf(".") + 1);

      try {
        service = (BaseServiceGeneriqueImpl) this
            .getService(nom, nomDefinition);
      } catch (Exception e) {
        if (log.isInfoEnabled()) {
          log.info("Erreur pour obtenir le service générique " + nomDefinition
              + ", utilisation du service générique. ", e);
        }
      }

      serviceTrouve = (service != null);

      if (!serviceTrouve) {
        classeATraiter = classeATraiter.getSuperclass();
        renduAObjetTransfert = classeATraiter.equals(ObjetTransfert.class);
      }
    }

    /*
     * Si aucun service générique ne correspond au nom de l'objet de transfert
     * ou à un de ces ancêtre, on doit alors utiliser le service générique par
     * défaut.
     */
    if (service == null) {
      service = (BaseServiceGeneriqueImpl) this
          .getService(
              premierNom,
              org.sofiframework.modele.adf.generique.utilitaire.UtilitaireServiceGenerique.DEFINITION_SERVICE_GENERIQUE);
    }

    /*
     * Si on trouve un service, on assigne la classe de l'objet de tranfert au
     * service
     */
    if (service != null) {
      boolean dejaInitialise = (service.getNomDefinitionAD() != null);

      /*
       * Il est important de ne pas réinitialiser un service pour rien.
       */
      if (!dejaInitialise) {
        /* Permet au service de déduire le nom de l'accès de données à utiliser. */
        service.specifierClasseObjetTransfertPourService(classeObjetTransfert);
      }
    }

    return service;
  }

  /**
   * Permet d'obtenir un service enfant du service en cours qui correspond au
   * nom de définition spécifié. Si le service n'existe pas, il est créé comme
   * un enfant du service courant.
   * 
   * @return Service enfant demandé
   * @param NomService
   *          Nom que doit porter le service
   * @param nomDefinition
   *          Nom complet du service. Le nom est composé du nom de package et du
   *          nom de l'interface du service.
   */
  public ApplicationModule getService(String nomService, String nomDefinition) {
    ApplicationModule service = null;

    if (nomDefinition != null) {
      // Essayer de trouver le service
      service = findApplicationModule(nomService);

      // Si il n'existe pas le créer comme service enfant du service courant.
      if (service == null) {
        try {
          service = createApplicationModule(nomService, nomDefinition);
        } catch (Exception e) {
          if (log.isInfoEnabled()) {
            log.info("Erreur pour obtenir le service générique demandé", e);
          }
        }
      }
    }

    return service;
  }

  /**
   * Permet d'ajouter dans un log la requêtre SQL au complet.
   * 
   * @param vue
   *          la vue dont on désire la requête SQL.
   */
  protected void debugSQL(BaseViewObjectImpl vue) {
    vue.debugSQL();
  }

  /**
   * Sauvegarde la liste d'objet de transferts existant, ajoute les nouveaux
   * objets et supprime les objet de transferts qui sont dans la base de donnée
   * selon la conditionSql, mais qui ne sont pas dans la nouvelle liste passé en
   * paramètre.
   * 
   * @param vue
   *          la vue à partir duquel modifier les données
   * @param liste
   *          la liste des nouveaux objets
   * @param conditionSql
   *          la condition SQL pour trouver les anciens objets
   * @param params
   *          les paramètres de la condition SQL
   * @throws ModeleException
   *           relance les exceptions du modèle.
   */
  protected void sauvegarderListe(BaseViewObjectImpl vue, List liste,
      String conditionSql, Object[] params) throws ModeleException {
    this.sauvegarderListe(vue, liste, conditionSql, params, null);
  }

  /**
   * Sauvegarde la liste d'objet de transferts existant, ajoute les nouveaux
   * objets et supprime les objet de transferts qui sont dans la base de donnée
   * selon la conditionSql, mais qui ne sont pas dans la nouvelle liste passé en
   * paramètre.
   * 
   * @param vue
   *          la vue à partir duquel modifier les données
   * @param liste
   *          la liste des nouveaux objets
   * @param conditionSql
   *          la condition SQL pour trouver les anciens objets
   * @param params
   *          les paramètres de la condition SQL
   * @param suppression
   *          traitement à exécuter avant la suppression d'une entité
   * @throws ModeleException
   *           relance les exceptions du modèle.
   */
  protected void sauvegarderListe(BaseViewObjectImpl vue, List liste,
      String conditionSql, Object[] params,
      TraitementAvantSuppression suppression) throws ModeleException {
    if (liste == null || liste.isEmpty()) {
      try {
        vue.supprimerRangee(this, conditionSql, params);
      } catch (ObjetTransfertNotFoundException e) {
      }
      return;
    } else {
      Class clazz = liste.get(0).getClass();
      ArrayList anciens = getObjetTransfertListe(vue, clazz, conditionSql,
          params, null, true);
      HashSet set = new HashSet();
      if (anciens != null) {
        for (Iterator it = anciens.iterator(); it.hasNext();) {
          ObjetTransfert objet = (ObjetTransfert) it.next();
          set.add(new Identifiant(objet));
        }
      }

      for (Iterator it = liste.iterator(); it.hasNext();) {
        ObjetTransfert objet = (ObjetTransfert) it.next();
        Identifiant id = new Identifiant(objet);

        if (!set.remove(id)) {
          vue.ajouterRangee(this, objet);
        } else {
          vue.modifierRangee(this, objet, id.getItems(), true);
        }
      }

      for (Iterator it = set.iterator(); it.hasNext();) {
        Identifiant id = (Identifiant) it.next();
        if (suppression != null) {
          suppression.executer(id);
        }
        vue.supprimerRangee(this, id.getItems(), true);
      }
    }
  }

  /**
   * Méthode pouvant être utilisé sur le modèle qui permet d'appliquer des
   * traitements spécifiques lors de la libération du modèle.
   */
  public void libererModele() {
  }

  /**
   * Retourne l'utilitaire qui permet de faciliter la génération de requête SQL.
   * 
   * @return l'utilitaire qui permet de faciliter la génération de requête SQL.
   */
  public UtilitaireSql getUtilitaireSql() {
    return UtilitaireSql.getInstance();
  }

  /**
   * Retourne la liste des données pour la requête SQL construite à partir de la
   * template velocity spécifiée en paramètre, ajuster par les paramètres de la
   * liste de navigation. Pour chaque ligne de résultat, une instance de
   * <code>Map</code> est créée pour contenir les valeurs des champs.
   * 
   * @param template
   *          le nom de la template Velocity à utiliser pour construire la
   *          requête SQL.
   * @param ln
   *          la liste de navigation
   * @return la liste de navigation avec les résultats de la recherche
   */
  protected ListeNavigation getListe(String template, ListeNavigation ln)
      throws ModeleException {
    String query = resoudreTemplateVelocity(template, ln);

    int count = getCount(query);
    query = getInterval(getOrderBy(query, ln), ln);

    ArrayList liste = getListe(query);
    ln.setListe(liste);
    ln.setNbListe(count);

    return ln;
  }

  /**
   * Exécute la requête SQL passé en paramètre et retourne les résultats dans
   * une liste de <code>Map</code> contenant la valeur des champs.
   * 
   * @param query
   *          la requête SQL à utiliser
   * @return une liste de <code>Map</code> contenant la valeur des champs.
   */
  protected ArrayList getListe(String query) throws ModeleException {
    ResultSet rs = null;
    Statement stmt = getDBTransaction().createStatement(0);

    try {
      rs = stmt.executeQuery(query);

      ArrayList liste = new ArrayList();
      ResultSetMetaData rsmd = rs.getMetaData();

      while (rs.next()) {
        Map map = new CaseInsensitiveMap();

        for (int i = 1, n = rsmd.getColumnCount(); i <= n; i++) {
          map.put(rsmd.getColumnName(i), rs.getObject(i));
        }

        liste.add(map);
      }

      return liste;
    } catch (SQLException e) {
      throw new ModeleException(e.toString(), e);
    } finally {
      close(rs);
      close(stmt);
    }
  }

  /**
   * Exécute une template Velocity à partir du context spécifié et retourne le
   * résultat.
   * 
   * @param template
   *          le nom d'une template Velocity
   * @param ctx
   *          un context Velocity
   * @return la template transformée
   */
  protected String resoudreTemplateVelocity(String template, Context ctx)
      throws ModeleException {
    try {
      Template t = Velocity.getTemplate(template);
      StringWriter sw = new StringWriter();
      ctx.put("esc", new EscapeTool());
      ctx.put("date", new DateTool());
      t.merge(ctx, sw);

      String retour = sw.toString();

      if (log.isDebugEnabled()) {
        log.debug("resoudreTemplateVelocity: \n" + retour);
      }

      return retour;
    } catch (Exception e) {
      throw new ModeleException(e.toString(), e);
    }
  }

  /**
   * Exécute une template Velocity avec le filtre de la liste de navigation dans
   * le contexte Velocity et retourne le résultat.
   * 
   * @param template
   *          le nom d'une template Velocity
   * @param ln
   *          une liste de navigation contenant un filtre
   * @return la template transformée
   */
  protected String resoudreTemplateVelocity(String template, ListeNavigation ln)
      throws ModeleException {
    Context ctx = new VelocityContext();
    ctx.put("filtre", ln.getObjetFiltre());

    return resoudreTemplateVelocity(template, ctx);
  }

  /**
   * Exécute une template Velocity et retourne le résultat.
   * 
   * @param template
   *          le nom d'une template Velocity
   * @return la template transformée
   */
  protected String resoudreTemplateVelocity(String template)
      throws ModeleException {
    return resoudreTemplateVelocity(template, new VelocityContext());
  }

  /**
   * Exécute un "select count(*) from ( ... )" avec la requête SQL passé en
   * paramètre et retourne le résultat.
   * 
   * @param query
   *          une requête SQL
   * @return le nombre d'élément retournés par la requête SQL
   */
  private int getCount(String query) throws ModeleException {
    ResultSet rs = null;
    Statement stmt = getDBTransaction().createStatement(0);

    try {
      rs = stmt.executeQuery("select count(*) from (" + query + ")");
      rs.next();

      return rs.getInt(1);
    } catch (SQLException e) {
      throw new ModeleException(e.toString(), e);
    } finally {
      close(rs);
      close(stmt);
    }
  }

  /**
   * Transforme la requête SQL pour retourner ajouter un tri selon ce qui est
   * définit dans la liste de navigation.
   * 
   * @param query
   *          la requête SQL
   * @param ln
   *          les paramètres de tri
   * @return la requête SQL transformée
   */
  private String getOrderBy(String query, ListeNavigation ln) {
    if (ln.getTriAttribut() == null) {
      return query;
    } else {
      return "select * from (\n\n" + query + "\n\n) order by "
          + ln.getTriAttribut() + " " + ln.getOrdreTri();
    }
  }

  /**
   * Transforme la requête SQL pour retourner seulement les données de la page
   * sélectionné par les paramètres de la liste de navigation.
   * 
   * @param query
   *          la requête SQL
   * @param ln
   *          les paramètres de la pagination
   * @return la requête SQL transformée
   */
  private String getInterval(String query, ListeNavigation ln) {
    int page = ln.getNoPage();
    int taille = ln.getMaxParPage().intValue();

    if (page == 1) {
      return "select * from (\n\n" + query + "\n\n) where rownum <= " + taille;
    } else {
      int offset = (page - 1) * taille;

      return "select * from ( " + "  select q.*, rownum row_order from (\n\n"
      + query + "\n\n) q " + ") where row_order between " + (1 + offset)
      + " and " + (offset + taille);
    }
  }

  /**
   * Ferme le <code>Statementt</code> sans retourner d'erreur.
   */
  protected static void close(Statement stmt) {
    UtilitaireJDBC.close(stmt);
  }

  /**
   * Ferme le <code>ResultSet</code> sans retourner d'erreur.
   */
  protected static void close(ResultSet rs) {
    UtilitaireJDBC.close(rs);
  }

  /**
   * Fixer la liste des attributs temporaires pour l'utilisation d'un service.
   * 
   * @param listeAttributTemporaire
   *          la liste des attributs temporaires pour l'utilisation d'un
   *          service.
   * @since 2.1
   */
  public void setListeAttributTemporaire(HashMap listeAttributTemporaire) {
    this.listeAttributTemporaire = listeAttributTemporaire;
  }

  /**
   * Retourne la liste des attributs temporaires pour l'utilisation d'un
   * service.
   * 
   * @return liste des attributs temporaires pour l'utilisation d'un service.
   * @since 2.1
   */
  public HashMap getListeAttributTemporaire() {
    return listeAttributTemporaire;
  }

  /**
   * Fixer une valeur d'un attribut qui est conservé temporairement lors de
   * l'utilisateur du service.
   * 
   * @param nom
   *          le nom de l'attribut
   * @param valeur
   *          la valeur de l'attribut.
   * @since 2.1
   */
  public void setAttributTemporaire(String nom, Object valeur) {
    getListeAttributTemporaire().put(nom, valeur);
  }

  /**
   * Retourne la valeur d'un attribut temporaire pour l'utilisation du service.
   * 
   * @param nom
   *          le nom de l'attribut temporaire.
   * @return la valeur de l'attribut temporaire.
   * @since 2.1
   */
  public Object getAttributTemporaire(String nom) {
    return getListeAttributTemporaire().get(nom);
  }

  /**
   * Fixe l'utilisateur en cours
   * 
   * @param gestionnaireUtilisateur Gestionnaire des utilisateurs
   * @since SOFI 3.0.2
   */
  @Override
  public void setGestionnaireUtilisateur(GestionnaireUtilisateur gestionnaireUtilisateur) {
    if (getDBTransaction().getSession().getUserData() != null && getDBTransaction().getSession().getUserData().get(Constantes.UTILISATEUR) == null) {
      Hashtable session = getDBTransaction().getSession().getUserData();
      session.put(Constantes.UTILISATEUR, gestionnaireUtilisateur);
    }
  }

  /**
   * Obtenir le gestionnaire des utilisateurs.
   * 
   * @return Gesitonnaire des utilisateurs
   * @since SOFI 3.0.2
   */
  @Override
  public GestionnaireUtilisateur getGestionnaireUtilisateur() {
    Object retour = getDBTransaction().getSession().getUserData().get(Constantes.UTILISATEUR);

    if (retour instanceof GestionnaireUtilisateur) {
      return (GestionnaireUtilisateur) retour;
    }else {
      return null;
    }
  }

  /**
   * Obtenir l'utilisateur en cours, si il s'agit d'un appel de service ou
   * l'utilisateur est authentifié.
   * @since SOFI 3.0.2
   * 
   * @return
   */
  @Override
  public Utilisateur getUtilisateurEnCours() {
    if (this.getGestionnaireUtilisateur() != null) {
      return this.getGestionnaireUtilisateur().getUtilisateur();
    }else {
      return (Utilisateur)getDBTransaction().getSession().getUserData().get(Constantes.UTILISATEUR);
    }
  }


  /**
   * Obtenir le code l'utilisateur en cours, si il s'agit d'un appel de service ou
   * l'utilisateur est authentifié.
   * 
   * @return Code utilisateur en cours de traitement.
   * @since SOFI 3.0.2
   */
  @Override
  public String getCodeUtilisateurEnCours() {
    return this.getUtilisateurEnCours().getCodeUtilisateur();
  }

  /**
   * Classe qui permet de définir une partie de traitement à exécuter avant la
   * suppression d'une entité.
   */
  public abstract class TraitementAvantSuppression {

    public TraitementAvantSuppression() {
    }

    /**
     * Exécution du traitement.
     * 
     * @param id
     *          Identifiant unique de l'entité qui sera supprimée
     */
    public abstract void executer(Identifiant id);
  }
}
