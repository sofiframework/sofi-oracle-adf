/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.adf.utilitaire;

import java.sql.CallableStatement;
import java.sql.SQLException;

import org.sofiframework.modele.exception.ModeleException;


/**
 * Classe utilitaire pour ADF BusinessComponent.
 * <p>
 * @author Jean-François Brassard (Nurun inc.)
 * @version SOFI 2.0
 */
public class UtilitairePLSQL {
  /**
   * Simplifie l'appel d'une procédure stocké BD avec des paramètres associé.
   *
   * Utiliser cette signature avec les types lorsque certains de vos paramètres peuvent être nuls.
   *
   * Vous pouvez utiliser les types de constantes NUMBER, DATE, et VARCHAR2 de la classe ConstantesSQL
   * pour le type de retour, sinon le type JDBC de retour sera java.sql.Types.
   *
   * @param typeSQLRetour le type de donnée JDBC de retour, voir ConstantesSQL pour les types disponibles.
   * @param positionParametreSortie la position du paramètre de sortie
   * @param positionPremierParametreEntree la position du premier paramètre en entrée.
   * @param fonction la fonction stocké BD.
   * @param variableAssocies  Un tableau d'objet de paramètre associé à la fonction.
   * @param typesAssocies Un tableau contenant les types des paramètres, voir ConstantesSQL pour les types disponibles.
   * @return Retourne la valeur de l'objet retour.
   * @throws ModeleException l'exception résultante de l'éxécution de la procédure.
   */
  public static Object executerFonctionPLSQL(int typeSQLRetour,
      int positionParametreSortie, int positionPremierParametreEntree,
      String fonction, Object[] variableAssocies, int[] typesAssocies,
      oracle.jbo.server.DBTransaction dbTransaction) throws ModeleException {
    CallableStatement st = null;

    StringBuffer fonctionComplete = new StringBuffer();

    if (fonction.toLowerCase().indexOf("begin") < 0) {
      fonctionComplete.append("begin ");
      fonctionComplete.append(fonction);
      fonctionComplete.append(" end;");
    } else {
      fonctionComplete.append(fonction);
    }

    try {
      st = dbTransaction.createCallableStatement(fonctionComplete.toString(), 0);
      st.registerOutParameter(positionParametreSortie, typeSQLRetour);

      if (variableAssocies != null) {
        for (int z = 0; z < variableAssocies.length; z++) {
          if (variableAssocies[z] == null) {
            st.setNull(z + positionPremierParametreEntree, typesAssocies[z]);
          } else {
            st.setObject(z + positionPremierParametreEntree, variableAssocies[z]);
          }
        }
      }

      st.executeUpdate();

      return st.getObject(positionParametreSortie);
    } catch (SQLException e) {
      throw new ModeleException(e);
    } finally {
      UtilitaireJDBC.close(st);
    }
  }

  /**
   * Simplife les appels à une procédure stocké BD avec des paramètres associé.
   *
   * Utiliser cette signature avec les types lorsque certains de vos paramètres peuvent être nuls.
   *
   * @param procedure la procédure stocké à exécuté.
   * @param positionPremierParametreEntree la position du premier paramètre en entrée.
   * @param variableAssocie le tableau d'objet associé à la procédure. Mettre null si aucun paramètre.
   * @param typesAssocies Un tableau contenant les types des paramètres, voir ConstantesSQL pour les types disponibles.
   * @throws ModeleException l'exception résultante de l'éxécution de la procédure.
   */
  protected void executerProcedurePLSQL(String procedure,
      int positionPremierParametreEntree, Object[] variableAssocie,
      int[] typesAssocies, oracle.jbo.server.DBTransaction dbTransaction)
          throws ModeleException {
    CallableStatement st = null;

    StringBuffer procedureComplete = new StringBuffer();

    if (procedure.toLowerCase().indexOf("begin") < 0) {
      procedureComplete.append("begin ");
      procedureComplete.append(procedure);
      procedureComplete.append(" end;");
    } else {
      procedureComplete.append(procedure);
    }

    try {
      if (variableAssocie != null) {
        st = dbTransaction.createCallableStatement(procedureComplete.toString(),
            0);

        if (variableAssocie != null) {
          for (int z = 0; z < variableAssocie.length; z++) {
            if (variableAssocie[z] == null) {
              st.setNull(z + positionPremierParametreEntree, typesAssocies[z]);
            } else {
              st.setObject(z + positionPremierParametreEntree,
                  variableAssocie[z]);
            }
          }
        }

        st.executeUpdate();
      } else {
        dbTransaction.executeCommand(procedureComplete.toString());
      }
    } catch (SQLException e) {
      throw new ModeleException(e.getMessage(), e);
    } finally {
      UtilitaireJDBC.close(st);
    }
  }

  /**
   * Simplifie l'appel d'une procédure stocké BD avec des paramètres associé.
   *
   * Utiliser cette signature avec les types lorsque certains de vos paramètres peuvent être nuls.
   *
   * Vous pouvez utiliser les types de constantes NUMBER, DATE, et VARCHAR2 de la classe ConstantesSQL
   * pour le type de retour, sinon le type JDBC de retour sera java.sql.Types.
   *
   * @param typeSQLRetour Un tableau contenant les types des données JDBC de retour, voir ConstantesSQL pour les types disponibles.
   * @param fonction la fonction stocké BD.
   * @param positionPremierParametreSortie la position du premier paramètre en sortie.
   * @param variableAssocies  Un tableau d'objet de paramètre associé à la fonction.
   * @param typesAssocies Un tableau contenant les types des paramètres, voir ConstantesSQL pour les types disponibles.
   * @return Retourne un tableau contenant les valeurs des objets de retour.
   * @throws ModeleException l'exception résultante de l'éxécution de la procédure.
   */
  public static Object[] executerProcedurePLSQL(int[] typeSQLRetour,
      String procedure, int positionPremierParametreSortie,
      Object[] variableAssocies, int[] typesAssocies,
      oracle.jbo.server.DBTransaction dbTransaction) throws ModeleException {
    CallableStatement st = null;

    int positionPremierParametreEntree = 1;

    if (positionPremierParametreSortie == 1) {
      positionPremierParametreEntree = typeSQLRetour.length + 1;
    }

    StringBuffer procedureComplete = new StringBuffer();

    Object[] valeursRetour = new Object[0];

    if (typeSQLRetour != null) {
      valeursRetour = new Object[typeSQLRetour.length];
    }

    if (procedure.toLowerCase().indexOf("begin") < 0) {
      procedureComplete.append("begin ");
      procedureComplete.append(procedure);
      procedureComplete.append(" end;");
    } else {
      procedureComplete.append(procedure);
    }

    try {
      st = dbTransaction.createCallableStatement(procedureComplete.toString(), 0);

      for (int y = 0; y < valeursRetour.length; y++) {
        st.registerOutParameter(positionPremierParametreSortie + y,
            typeSQLRetour[y]);
      }

      if (variableAssocies != null) {
        for (int z = 0; z < variableAssocies.length; z++) {
          if (variableAssocies[z] == null) {
            st.setNull(z + positionPremierParametreEntree, typesAssocies[z]);
          } else {
            st.setObject(z + positionPremierParametreEntree, variableAssocies[z]);
          }
        }
      }

      st.executeUpdate();

      for (int x = 0; x < valeursRetour.length; x++) {
        valeursRetour[x] = st.getObject(positionPremierParametreSortie + x);
      }

      return valeursRetour;
    } catch (SQLException e) {
      throw new ModeleException(e.getMessage(), e);
    } finally {
      UtilitaireJDBC.close(st);
    }
  }
}
