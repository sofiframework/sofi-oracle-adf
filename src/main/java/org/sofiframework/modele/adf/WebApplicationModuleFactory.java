/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.adf;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import oracle.jbo.ApplicationModule;
import oracle.jbo.http.HttpUtil;

import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.modele.Modele;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class WebApplicationModuleFactory extends ApplicationModuleFactory {

  public ApplicationModule get(HttpServletRequest request) {
    ApplicationModule am = this.get();

    this.initialiserLocale(am, request);
    this.initialiserUtilisateur(am, request);

    return am;
  }

  private void initialiserLocale(ApplicationModule am, HttpServletRequest request) {
    Locale locale = HttpUtil.determineLocale(request);

    // Configurer l'application module avec le locale de l'utilisateur
    if (locale != null && am.getSession() != null) {
      am.getSession().setLocale(locale);
    }
  }

  private void initialiserUtilisateur(ApplicationModule am, HttpServletRequest request) {
    if (am instanceof BaseServiceImpl) {
      BaseServiceImpl service = (BaseServiceImpl) am;
      Utilisateur utilisateur = UtilitaireControleur.getUtilisateur(request.getSession());

      // Fixer l'utilisateur dans le modèle si ce n'est déjà fait.
      if ((utilisateur != null) &&
          ((service.getUtilisateur() == null) ||
              (service.getUtilisateur().getCodeUtilisateur() == null))) {
        service.setUtilisateur(utilisateur);
      }

      if (utilisateur == null) {
        utilisateur = new Utilisateur();
        service.setUtilisateur(utilisateur);
      }
    }
  }

  public void releaseApplicationModuleCourant(HttpServletRequest request) {
    ApplicationModule am = this.getApplicationModuleCourant();
    am.getSession().setLocale(null);
    if (am instanceof BaseServiceImpl) {
      BaseServiceImpl service = (BaseServiceImpl) am;
      service.setUtilisateur(null);
    }
  }

  /**
   * Obtenir le modèle statiquement à partir du contexte
   * Spring en cours.
   * @param nomFactory Nom du factory adf dans le contexte
   * @param request Requête http
   * @return modele
   */
  public static Modele getModeleStatic(String nomFactory, HttpServletRequest request) {
    ApplicationContext contexte = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
    ApplicationModuleFactory factory = (ApplicationModuleFactory) contexte.getBean(nomFactory);
    return (Modele) factory.getStatic();
  }
}
