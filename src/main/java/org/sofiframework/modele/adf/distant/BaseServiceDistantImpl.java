/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.adf.distant;

import javax.ejb.EJBObject;

import org.sofiframework.modele.adf.BaseServiceImpl;
import org.sofiframework.modele.distant.GestionServiceDistant;


/**
 * Implantation de base d'un service distant (EJB) .
 * <p>
 * @author Jean-Maxime Pelletier, Nurun inc.
 * @author Jean-François Brassard (Nurun inc.)
 * @version 1.0
 */
public abstract class BaseServiceDistantImpl extends BaseServiceImpl {
  /** Constructeur par défaut */
  public BaseServiceDistantImpl() {
  }

  /**
   * Obtenir un service distant à partir de son nom.
   * @param nom Le nom du service recherché.
   * @return Un service distant EJB
   */
  @Override
  public EJBObject getServiceDistant(String nom) {
    return (EJBObject) GestionServiceDistant.getServiceDistant(nom);
  }
}
