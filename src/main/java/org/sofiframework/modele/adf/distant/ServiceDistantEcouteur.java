/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.modele.adf.distant;

import java.util.EventObject;

import oracle.jbo.ComponentObjectListener;
import oracle.jbo.server.ContainerObjectEvent;
import oracle.jbo.server.ContainerObjectListener;

/**
 * Ecoute les évènements produit par les instances de ApplicationModuleImpl.
 * <p>
 * @author Jean-Maxime Pelletier, Nurun inc.
 * @version 1.0
 */
public class ServiceDistantEcouteur implements ContainerObjectListener,
ComponentObjectListener {

  /** Constructeur par défaut */
  public ServiceDistantEcouteur() {
  }

  /**
   * Traitement effectuer lors de l'ajout d'une composante au service distant.
   * @param e Évènement
   */
  @Override
  public void componentAdded(ContainerObjectEvent e) {
  }

  /**
   * Traitement effectué lorsqu'une composante est retirée du service distant.
   * @param e Évènement
   */
  @Override
  public void componentRemoved(ContainerObjectEvent e) {
  }

  /**
   * Traitement effectué lorsqu'une exception survient dans lors de
   * l'exécution du service.
   * @param e Évènement
   */
  @Override
  public void exceptionOccured(Exception e) {
  }

  /**
   * Traitement effectué à tous les évènement survenant dans le servide distant.
   * @param event Évènement
   */
  @Override
  public void eventOccured(EventObject event) {
  }
}